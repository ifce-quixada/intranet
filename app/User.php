<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPassword;

use Laravel\Passport\HasApiTokens;
use LdapRecord\Laravel\Auth\AuthenticatesWithLdap;
use LdapRecord\Laravel\Auth\LdapAuthenticatable;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable
{
    use Notifiable, AuthenticatesWithLdap, HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_type', 'username', 'email', 'password', 'role', 'meta',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attribute meta should be treat as array.
     *
     * @var array
     */
    protected $casts = [
      'id' => 'int',
      'meta' => 'array',
    ];

    /**
      * Send a password reset email to the user
      */

    public function sendPasswordResetNotification($token)
    {
          $this->notify(new MailResetPassword($token));
    }

    /**
     ** Get the activities for the user.
     **/

    public function activities() {
        return $this->hasMany('App\Models\Activity');
    }

}
