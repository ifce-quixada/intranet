<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
     'user_id',
     'type',
     'description',
     'workload',
     'course',
     'semester',
     'meta'
   ];

    /**
     * The attribute meta should be treat as array.
     *
     * @var array
     */
    protected $casts = [
      'id' => 'int',
      'meta' => 'array',
    ];

   /**
    * Get the post that owns the comment.
    */
   public function post() {
    return $this->belongsTo('App\User');
   } 

   /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y G:i', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y G:i', strtotime($value));
    }
}
