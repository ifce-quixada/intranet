<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $data)
    {
      $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      if ($this->data['queue'] == 'ti') {
      return $this->subject($this->data['subject'])
                    ->from($this->data['email'], $this->data['name'])
                    ->replyTo($this->data['email'], $this->data['name'])
                    ->to(env('TI_MAIL_QUEUE'))
                    ->view('tickets.ticketmail-ti');
      } else {
      return $this->subject($this->data['subject'])
                    ->from($this->data['email'], $this->data['name'])
                    ->replyTo($this->data['email'], $this->data['name'])
                    ->to(env('INFRA_MAIL_QUEUE'))
                    ->view('tickets.ticketmail-infra');
      }
    }
}
