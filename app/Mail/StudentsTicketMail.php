<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentsTicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $data, $filesPath)
    {
        $this->data = $data;
        $this->filesPath = $filesPath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Solicitação de Diploma' . ' - ' . strtoupper($this->data['name']);
        if ($this->data['queue'] == 'cca') {
            $email = $this->view('tickets.ticketmail-cca')
                ->subject($subject)
                ->from($this->data['email'])
                ->replyTo($this->data['email'], $this->data['name'])
                ->to(env('CCA_MAIL_QUEUE'));

            foreach($this->filesPath as $fp) {
                $email->attachFromStorage($fp, explode('/', $fp)[1], ['mime' => 'application/pdf']);
            }

            return $email;
        }
    }
}
