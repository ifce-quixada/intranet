<?php

namespace App\Listeners;

use LdapRecord\Laravel\Events\Imported;

class ImportedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Imported  $event
     * @return void
     */
    public function handle(Imported $event)
    {
      $event->model->work_schedule = 'SEG: 09h - 12h / 13h - 18h TER-SEX: 07h - 12h / 13h - 16h';
      $event->model->work_shift = 'm';
      $event->model->work_load = '40';
      $event->model->save();
    }
}
