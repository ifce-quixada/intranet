<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\Setting;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{

    /**
     * Display index.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $links = Link::all();
        $setting = Setting::orderBy('created_at', 'DESC')->first();
        return view('welcome', compact('links', 'setting'));
    }

}
