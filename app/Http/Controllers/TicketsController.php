<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Mail\TicketMail;
use Illuminate\Http\Request;
use Mail;

class TicketsController extends Controller
{

    /**
     * Display index.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('tickets.index');
    }

    /**
     * Display IT index.
     *
     * @return Illuminate\View\View
     */
    public function ti()
    {
        return view('tickets.ti');
    }

    /**
     * Display IT index.
     *
     * @return Illuminate\View\View
     */
    public function infra()
    {
        return view('tickets.infra');
    }

    /**
     * Send e-mail to specified sector.
     *
     * @param Request $request 
     *
     * @return Illuminate\View\View
     */
    public function send(Request $request)
    {
      // if sector == 'ti' -> ticket is for it
      // if sector == 'infra' -> ticket is for infra
      
      $data = $this->getData($request);
      Mail::send(new TicketMail($data));

      /*
      if ($data['queue'] == 'ti') {
        Mail::send(new TicketMail($data));
      } else if ($data['queue'] == 'infra') {
        // prepare pdf and send it as attachament
        // Mail::send(new TicketMail($data));
        //

        $localtime = Carbon::now('America/Fortaleza');
        $pdf  =  \App::make('dompdf.wrapper');
        $view =  \View::make('tickets.os')
          ->with('data', $data)
          ->with('dt_request', $localtime);

        $pdf->loadHTML($view);

        $pdf_filename = 'os_' . $data['login'] . '_' . $localtime->timestamp . '.pdf';

        return $pdf->download($pdf_filename);
        dd($data);
      }
       */

      return redirect()->route('welcome')
        ->with('status_message', 'Seu chamado foi aberto com sucesso.')
        ->with('type_message', 'success');
    }

    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'queue' => 'required',
            'sector' => 'required',
            'name' => 'required|string|min:1|max:191',
            'login' => 'required',
            'meta' => 'nullable',
            'chief' => 'nullable',
            'emailto' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'number' => 'nullable',
            'content' => 'required',
        ];
        
        $data = $request->validate($rules);
        return $data;
    }

}
