<?php

namespace App\Http\Controllers;

use App\Models\Informative;
use Illuminate\Http\Request;

class InformativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informatives = Informative::orderBy('published_at', 'DESC')->get();
        return view('informatives.index', compact('informatives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function show(Informative $informative)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function edit(Informative $informative)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informative $informative)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informative $informative)
    {
        //
    }
}
