<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;
use App\Models\Activity;
use Illuminate\Http\Request;

use Auth;
use Exception;

class ManagementController extends Controller
{
   /**
     * Display dashboard.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
      $role = Auth::user()->role;

      if ($role == 'gestor' or $role == 'cordenador') {

        $numberOfTeachers = User::where(['user_type' => 'docente'])->get()->count();
        $numberOfTaes= User::where(['user_type' => 'tae'])->get()->count();
        $numberOfStudents = User::where(['user_type' => 'aluno'])->get()->count();
        $numberOfActivities = Activity::count();
        
        $teachersUsers = User::where(['user_type' => 'docente'])
          ->with('activities')
          ->get();

        $nFICActivity = Activity::where(['type' => 'FIC'])->get()->count();
        $nManutencaoActivity = Activity::where(['type' => 'Manutenção ao Ensino'])->get()->count();
        $nApoioActivity = Activity::where(['type' => 'Apoio ao Ensino'])->get()->count();
        $nOrientacaoActivity = Activity::where(['type' => 'Orientação'])->get()->count();
        $nEnsinoActivity = Activity::where(['type' => 'Ensino extracurricular'])->get()->count();
        $nPesquisaActivity = Activity::where(['type' => 'Pesquisa aplicada'])->get()->count();
        $nExtensaoActivity = Activity::where(['type' => 'Extensão'])->get()->count();
        $nGestaoActivity = Activity::where(['type' => 'Gestão'])->get()->count();
        $nComissoesActivity = Activity::where(['type' => 'Comissões ou Fiscalização'])->get()->count();
        $user = Auth::user();       
        $setting = Setting::get()->first();

        return view('managements.index', compact('user', 'teachersUsers',
          'numberOfTeachers', 'numberOfTaes', 'numberOfStudents',
          'numberOfActivities', 'nFICActivity', 'nManutencaoActivity', 
          'nApoioActivity', 'nOrientacaoActivity', 'nEnsinoActivity', 'nPesquisaActivity',
          'nExtensaoActivity', 'nGestaoActivity', 'nComissoesActivity',
          'setting'));
      }

      if ($role != 'gestor' or $role != 'coordenador') {
          return redirect()->route('welcome')
            ->with('status_message', 'Você não tem permissão para acessar esse módulo.')
            ->with('type_message', 'danger');
      }
    }

    /**
     * Display the specified link.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {

      $role = Auth::user()->role;

      if ($role == 'gestor' or $role == 'cordenador') {

        $teacher = User::findOrFail($id);
        $activities = Activity::where(['user_id' => $id ])->get();
        $setting = Setting::get()->first();

        return view('managements.show', compact('teacher', 'activities', 'setting'));
      } 

      if ($role != 'gestor' or $role != 'coordenador') {
          return redirect()->route('welcome')
            ->with('status_message', 'Você não tem permissão para acessar esse módulo.')
            ->with('type_message', 'danger');
      }

    }




}
