<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Teacher;
use App\Models\Activity;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Exception;

class TeachersController extends Controller
{

    /**
     * Display a listing of the teachers.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {

      $user_type = Auth::user()->user_type;
      $setting = Setting::get()->first();

      if ($user_type == 'docente') {

        $activities = Activity::where([
            ['user_id', '=', Auth::user()->id], 
            ['semester', '=', $setting->current_semester]])
          ->orderBy('created_at', 'desc')
          ->get();
        return view('teachers.index', compact('activities', 'setting'));

      } else {
        return redirect()->route('welcome')
          ->with('status_message', 'Você não tem permissão para acessar esse módulo.')
          ->with('type_message', 'danger');
      }

    }

    /**
     * Show the form for creating a new teacher.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a new teacher in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Teacher::create($data);

            return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Teacher was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified teacher.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $teacher = Teacher::findOrFail($id);
        $setting = Setting::get()->first();
        return view('teachers.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified teacher.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
      $teacher = Auth::user();
      $setting = Setting::get()->first();
      return view('teachers.edit', compact('teacher', 'setting'));
    }

    /**
     * Update the specified teacher in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        //try {
            
            $data = $this->getData($request);
            
            $teacher = User::findOrFail($id);
            $teacher->update($data);

            return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Informações atualizadas com sucesso.');

        //} catch (Exception $exception) {

        //    return back()->withInput()
        //                 ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        //}        
    }

    /**
     * Remove the specified teacher from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $teacher = Teacher::findOrFail($id);
            $teacher->delete();

            return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Teacher was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'birthday' => 'required|string|min:1|max:191',
            'regime' => 'required|string|min:1|max:191',
            'workload' => 'required|string|min:1|max:191',
            'phone' => 'required|string|min:1|max:191',
            'department' => 'required|string|min:1|max:191',
            'responsability' => 'required|string|min:1|max:191',
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
