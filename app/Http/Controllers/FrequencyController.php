<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Exception;

class FrequencyController extends Controller
{
    /**
     * Display a form to generate frequency.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
      $user_type = Auth::user()->user_type;

      if ($user_type == 'Técnico-administrativo') {
        $user = Auth::user();       
        return view('frequencies.index', compact('user'));
      } else {
        return redirect()->route('welcome')
          ->with('status_message', 'Você não tem permissão para acessar esse módulo.')
          ->with('type_message', 'danger');
      }

    }

    /**
     * Generate frequency.
     *
     * @return Illuminate\View\View
     */
    public function store(Request $request)
    {
      $data = $request->all();

      $user = User::where('id', Auth::user()->id)->first();
      $user['work_schedule'] = $data['work_schedule']; 
      $user['work_shift'] = $data['work_shift']; 
      $user['work_load'] = $data['work_load']; 
      $user->save();

      $months = [1 => "Janeiro", 2 => "Fevereiro", 3 => "Março",
                 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho",
                 8 => "Agosto", 9 => "Setembro", 10 => "Outubro",
                 11 => "Novembro", 12 => "Dezembro"];
      $data['month_name'] = strtoupper($months[$data['month']]);

      $dt = Carbon::createFromDate($data['year'], $data['month'], 1, 'America/Fortaleza');

      if ($data['vacation'] == "s") {
        $startVacation = Carbon::createFromFormat('d/m/Y', $data['start-vacation'], 'America/Fortaleza');
        /*$endVacation = Carbon::createFromFormat('d/m/Y', $data['end-vacation'], 'America/Fortaleza')->addDay();*/
        $endVacation = Carbon::createFromFormat('d/m/Y', $data['end-vacation'], 'America/Fortaleza');
      } else {
        $startVacation = Carbon::createFromTimestamp(-1);
        $endVacation = Carbon::createFromTimestamp(-1);
      } 

      $pdf  =  \App::make('dompdf.wrapper');
      $view =  \View::make('frequencies.report')
        ->with('data', $data)
        ->with('dt', $dt)
        ->with('startVacation', $startVacation)
        ->with('endVacation', $endVacation);

      $pdf->loadHTML($view);

      $localtime = Carbon::now('America/Fortaleza');
      $pdf_filename = 'frequencia_' . $data['username'] . '_' . $localtime->timestamp . '.pdf';


      return $pdf->download($pdf_filename);
    }

}
