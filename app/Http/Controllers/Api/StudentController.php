<?php

namespace App\Http\Controllers\Api;

use Mail;
use Carbon\Carbon;
use App\Mail\StudentsTicketMail;
use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::orderBy('name', 'ASC')->get();
        return response()->json($students);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($student_registration)
    {
        $student = Student::where('student_registration', '=', $student_registration)->firstOrFail();
        // TODO verify which fields are empty and let the CCA knows
        return response()->json($student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }

    /**
     * Send e-mail to Request Degree.
     *
     * @param Request $request
     *
     * @return Illuminate\View\View
     */

    public function sendRequestDegree(Request $request)
    {
        // if sector == 'cca' -> ticket is for cca

        $validator = $this->validateData($request);
        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json([
                'status_code' => '400',
                'status_message' => 'Todos os campos são obrigatórios.',
                'type_message' => 'is-warning',
                $messages]);
        }

        $filesPath = [];
        $f = $request->file('files');
        for ($k = 0; $k < count($f); $k++) {
            $filesPath[] = $f[$k]->storeAs('degree_requests', $k . '-' . $request->registration . '.pdf');
        }
        // $request->merge(['files' => $filesPath]);

        Mail::send(new StudentsTicketMail($request->all(), $filesPath));

        Storage::delete($filesPath);
        return response()->json([
            'status_code' => '200',
            'status_message' => 'Seu chamado foi aberto com sucesso.',
            'type_message' => 'is-success'
        ]);
    }

    /**
     * Export internship documentation as PDF.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function printInternshipDocs(Request $request)
    {
        $data = $request->all();

        $pdf  =  \App::make('dompdf.wrapper');
        if ($data["stage"] == "inicio") {
            $view =  \View::make('students.begin_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "durante") {
            $view =  \View::make('students.during_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "fim") {
            $view =  \View::make('students.ending_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "fim-empresa") {
            $view =  \View::make('students.ending_company_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "relatorio-mensal") {
            $view =  \View::make('students.during_student_report_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "termo-aditivo") {
            $view =  \View::make('students.during_contract_additive_internshipdocs')
                ->with('data', $data);
        } else if ($data["stage"] == "relatorio-final") {
            $view = \View::make('students.ending_final_report_internshipdocs')
                ->with('data', $data);
        }

        $pdf->loadHTML($view);
        $localtime = Carbon::now('America/Fortaleza');
        $pdf_filename = 'documentacao_estagio' . $data['studentRegistration'] . '_' . $localtime->timestamp . '.pdf';

        return $pdf->download($pdf_filename);
    }


	/**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function validateData(Request $request)
    {
        $rules = [
            'queue' => 'required',
            'name' => 'required',
            'course' => 'required',
            'registration' => 'required',
            'emailto' => 'required',
            'email' => 'required',
            'phone' => 'nullable',
            'request' => 'required',
            'files' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }
}
