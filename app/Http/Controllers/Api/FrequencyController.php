<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $d = [
            'name' => $request->name,
            'username' => $request->username,
            'work_schedule' => $request->work_schedule,
            'sector' => $request->sector,
        ];
        if ($request->vacation == 's') {
            $d['start_vacation'] = $request->start_vacation;
            $d['end_vacation'] = $request->end_vacation;

        }
        return response()->json($d);
         */

        $data = $request->all();

        $user = User::where('id', Auth::user()->id)->first();
        $user['work_schedule'] = $data['work_schedule'];
        $user['work_shift'] = $data['work_shift'];
        $user['work_load'] = $data['work_load'];
        $user->save();

        $months = [
            1 => "Janeiro",
            2 => "Fevereiro",
            3 => "Março",
            4 => "Abril",
            5 => "Maio",
            6 => "Junho",
            7 => "Julho",
            8 => "Agosto",
            9 => "Setembro",
            10 => "Outubro",
            11 => "Novembro",
            12 => "Dezembro"
        ];
        $data['month_name'] = strtoupper($months[$data['month']]);

        $dt = Carbon::createFromDate($data['year'], $data['month'], 1, 'America/Fortaleza');

        if ($data['vacation'] == "s") {
            $startVacation = Carbon::createFromFormat('d/m/Y', $data['start_vacation'], 'America/Fortaleza');
            $endVacation = Carbon::createFromFormat('d/m/Y', $data['end_vacation'], 'America/Fortaleza');
        } else {
            $startVacation = Carbon::createFromTimestamp(-1);
            $endVacation = Carbon::createFromTimestamp(-1);
        }

        $pdf  =  \App::make('dompdf.wrapper');
        $view =  \View::make('frequencies.report')
            ->with('data', $data)
            ->with('dt', $dt)
            ->with('startVacation', $startVacation)
            ->with('endVacation', $endVacation);

        $pdf->loadHTML($view);
        $localtime = Carbon::now('America/Fortaleza');
        $pdf_filename = 'frequencia_' . $data['username'] . '_' . $localtime->timestamp . '.pdf';

        return $pdf->download($pdf_filename);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
