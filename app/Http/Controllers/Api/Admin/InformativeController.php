<?php

namespace App\Http\Controllers\Api\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Informative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InformativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informatives = Informative::orderBy('published_at', 'ASC')->get();
        return response()->json($informatives);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // API doesn't have this method
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aux = Carbon::parse($request->published_at);
        $request->merge(['published_at' => $aux]);

        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        Informative::create($request->all());
        return response()->json(['msg' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function show($informative_id)
    {
        // "published_at": "2020-09-07 00:00:00",
        $informative = Informative::where('id', '=', $informative_id)->firstOrFail();
        $aux = Carbon::createFromFormat('Y-m-d H:i:s', $informative->published_at, 'America/Fortaleza');
        $informative->published_at = $aux->toIso8601String();

        return response()->json($informative);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function edit(Informative $informative)
    {
        // API doesn't have this method
        $informative = Informative::findOrFail($informative->id);
        return view('admin.informatives.edit', compact('informative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informative $informative)
    {

        $aux = Carbon::parse($request->published_at);
        $request->merge(['published_at' => $aux]);

        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        $informative->update($request->all());
        return response()->json($informative);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informative $informative)
    {
        $informative->delete();
        return response()->json($informative);
    }

    protected function validateData(Request $request)
    {
        $rules = [
            'title' => 'required|string|min:1|max:191',
            'link' => 'required|string|min:1',
            'published_at' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }

}
