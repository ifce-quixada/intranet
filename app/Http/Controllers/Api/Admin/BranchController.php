<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::orderBy('branch_line', 'ASC')->get();
        return response()->json($branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // API doesn't have this method
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        Branch::create($request->all());
        return response()->json(['msg' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show($branch_id)
    {
        $branch = Branch::where('id', '=', $branch_id)->firstOrFail();
        return response()->json($branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        // API doesn't have this method
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {

        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        $branch->update($request->all());

        return response()->json($branch);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        return response()->json($branch);
    }

    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function validateData(Request $request)
    {
        $rules = [
            'sector' => 'required|string|min:1|max:191',
            'contacts' => 'required|string|min:1|max:191',
            'branch_line' => 'required|string|min:1|max:4',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }

}
