<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::orderBy('current_semester', 'DESC')->get();
        return response()->json($settings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // API doesn't have this method
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        Setting::create($request->all());
        return response()->json(['msg' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show($setting_id)
    {

        $setting = Setting::where('id', '=', $setting_id)->firstOrFail();
        return response()->json($setting);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        // API doesn't have this method
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
            $messages = $validator->errors()->all();
            return response()->json($messages);
        }

        $setting->update($request->all());
        return response()->json($setting);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        $setting->delete();
        return response()->json($setting);
    }

	/**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function validateData(Request $request)
    {
        $rules = [
            'current_semester' => 'required|string|min:1|max:191',
            'first_school_day' => 'required|string|min:1|max:191',
            'last_school_day' => 'required|string|min:1|max:191',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }
}
