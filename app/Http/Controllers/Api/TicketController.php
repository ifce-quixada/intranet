<?php

namespace App\Http\Controllers\Api;

use Mail;
use App\Mail\TicketMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Send e-mail to specified sector.
     *
     * @param Request $request
     *
     * @return Illuminate\View\View
     */
    public function send(Request $request)
    {
        // if sector == 'ti' -> ticket is for it
        // // if sector == 'infra' -> ticket is for infra

        $data = $this->getData($request);
         Mail::send(new TicketMail($data));

        return response()->json([
            'status_message' => 'Seu chamado foi aberto com sucesso.',
            'type_message' => 'is-success'
        ]);
    }

    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'queue' => 'required',
            'sector' => 'required',
            'name' => 'required|string|min:1|max:191',
            'login' => 'required',
            'meta' => 'nullable',
            'chief' => 'nullable',
            'emailto' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'number' => 'nullable',
            'content' => 'required',
        ];

        $data = $request->validate($rules);
        return $data;
    }

}
