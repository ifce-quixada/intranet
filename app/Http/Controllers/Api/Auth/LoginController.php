<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use LdapRecord\Laravel\Auth\ListensForLdapBindFailure;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, ListensForLdapBindFailure;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->listenForLdapBindFailure();
    }

    public function username()
    {
      return 'username';
    }

    protected function credentials(Request $request)
    {
        return [
            'samaccountname' => $request->get('username'),
            'password' => $request->get('password'),
        ];
    }

    public function login(Request $request)
    {
		if (Auth::attempt(['samaccountname' => $request->username, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('intranet')->accessToken;
            return response()->json(['success' => $success, 'user' => $user], 200);
        }
        else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
