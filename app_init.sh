#!/bin/sh

php artisan key:generate
php artisan storage:link
php artisan passport:client --personal --no-interaction
php artisan passport:keys
php artisan migrate
