<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $this->faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($this->faker));
        $this->faker->addProvider(new \Faker\Provider\pt_BR\Person($this->faker));
        return [
            'name' => $this->faker->name,
            'student_registration' => $this->faker->isbn13,
            'cpf' => $this->faker->cpf,
            'birthday' => $this->faker->dateTimeBetween(),
            'course' => $this->faker->word,
            'email_personal' => $this->faker->safeEmail,
            'phone_1' => $this->faker->cellphoneNumber(false),
            'campus' => $this->faker->city
        ];
    }
}
