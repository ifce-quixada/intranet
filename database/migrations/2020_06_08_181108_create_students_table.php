<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('student_registration')->unique();
            $table->string('cpf')->unique();
            $table->string('birthday');
            $table->string('course');
            $table->string('email_ifce')->unique()->nullable();
            $table->string('email_qacademico')->nullable();;
            $table->string('email_personal_secondary')->nullable();
            $table->string('email_personal');
            $table->string('address')->nullable();
            $table->string('street_number')->nullable();
            $table->string('address_suplement')->nullable();
            $table->string('address_neighborhood')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('campus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
