<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $setting = new Setting;
      $setting->current_semester = '2018.1';
      $setting->first_school_day = '01/01/2018';
      $setting->last_school_day = '30/12/2018';
      $setting->save();
    }
}
