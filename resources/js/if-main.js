window.onclick = e => {
  var targetElem = (e.srcElement.getAttribute('data-target'))
  if (targetElem != null) {
    var modalElem = document.getElementById(targetElem)
    if (modalElem.classList.contains('is-active')) {
      modalElem.classList.remove('is-active')
    } else {
      document.getElementById(targetElem).classList.add('is-active')
    }
  }
}


