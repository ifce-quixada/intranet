<div id="form-docente" hidden>
  <div class="">

  <div class="field">
    <label class="label">SIAPE</label>
    <input 
      id="docente-login" 
      type="text" 
      class="input {{ $errors->has('login') ? ' is-danger' : '' }}" 
      name="login" 
      value="{{ old('login') }}" 
      required>

    @if ($errors->has('login'))
      <p class="help is-danger">
        {{ $errors->first('login') }}
      </p>
    @endif
  </div> <!-- field docente-login -->

  <div class="field">
    <label class="label">Nome completo</label>
    <input 
      id="docente-name" 
      type="text" 
      class="input {{ $errors->has('name') ? ' is-danger' : '' }}" 
      name="name" 
      value="{{ old('name') }}" 
      required>

    @if ($errors->has('name'))
      <p class="help is-danger">
        {{ $errors->first('name') }}
      </p>
    @endif
  </div> <!-- field docente-name -->

  <div class="uk-tile ii-tile-warning uk-padding-small">
  <div class="field">
    <label class="label">Carga horária em sala de aula no semestre atual</label>
    <input 
      id="docente-workload_in_semester" 
      type="text" 
      class="input {{ $errors->has('workload_in_semester') ? ' is-danger' : '' }}" 
      name="workload_in_semester" 
      value="{{ old('workload_in_semester') }}" 
      required>

    @if ($errors->has('workload_in_semester'))
      <p class="help is-danger">
        {{ $errors->first('workload_in_semester') }}
      </p>
    @endif
  </div> <!-- field docente-name -->
  </div> <!-- uk-tile -->

  <div class="field uk-grid-small uk-child-width-auto uk-grid">
    <label class="label">Carga horária</label>
    <label><input class="uk-radio" type="radio" name="workload" value="40" checked> 40 horas</label>
    <label><input class="uk-radio" type="radio" name="workload" value="20"> 20 horas</label>
  </div> <!-- field workload -->

  <div class="field uk-grid-small uk-child-width-auto uk-grid">
    <label class="label">Regime</label>
    <label><input class="uk-radio" type="radio" value="Efetivo" name="regime" checked> Efetivo</label>
    <label><input class="uk-radio" type="radio" value="Substituto" name="regime"> Substituto</label>
    <label><input class="uk-radio" type="radio" value="Convidado" name="regime"> Convidado</label>
  </div> <!-- field regime -->

  <div class="field uk-grid-small uk-child-width-auto uk-grid">
    <label class="label">Cargo</label>
    <label><input class="uk-radio" type="radio" value="Professor" name="responsability" checked> Professor</label>
    <label><input class="uk-radio" type="radio" value="Coordenador" name="responsability"> Coordenador</label>
    <label><input class="uk-radio" type="radio" value="Diretor" name="responsability"> Diretor</label>
  </div> <!-- field responsability -->

  <div class="field">
    <label class="label">Data de aniversário</label>
    <input id="docente-birthday" type="text" class="input {{ $errors->has('birthday') ? ' is-danger' : '' }}" name="birthday" value="{{ old('birthday') }}" required>

    @if ($errors->has('birthday'))
      <p class="help is-danger">
        {{ $errors->first('birthday') }}
      </p>
    @endif
  </div> <!-- field birthday -->

  <div class="field">
    <label class="label">Telefone</label>
    <input 
      id="docente-phone" 
      type="text" 
      class="input {{ $errors->has('phone') ? ' is-danger' : '' }}" 
      name="phone" 
      value="{{ old('phone') }}" 
      required>

    @if ($errors->has('phone'))
      <p class="help is-danger">
        {{ $errors->first('phone') }}
      </p>
    @endif
  </div> <!-- field docente-phone -->

  <div class="field">
    <label class="label">Departamento</label>
    <input 
      id="docente-department" 
      type="text" 
      class="input {{ $errors->has('department') ? ' is-danger' : '' }}" 
      name="department" 
      value="{{ old('department') }}" 
      required>

    @if ($errors->has('department'))
      <p class="help is-danger">
        {{ $errors->first('department') }}
      </p>
    @endif
  </div> <!--field docente-department -->

  <div class="field">
    <label class="label">Email</label>
    <input 
      id="docente-email" 
      type="email" 
      class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
      name="email" 
      value="{{ old('email') }}" 
      required>

    @if ($errors->has('email'))
      <p class="help is-danger">
        {{ $errors->first('email') }}
      </p>
    @endif
  </div> <!-- field docoente-email -->

  <div class="field">
    <label class="label">Senha</label>
    <input 
      id="docente-password" 
      type="password" 
      class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
      name="password" 
      value="{{ old('password') }}" 
      required>

    @if ($errors->has('password'))
      <p class="help is-danger">
        {{ $errors->first('password') }}
      </p>
    @endif
  </div> <!-- field docente-password -->

  <div class="field">
    <label class="label">Confirmar senha</label>
    <input 
      id="docente-password_confirmation" 
      type="password" 
      class="input {{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" 
      name="password_confirmation" 
      value="{{ old('password_confirmation') }}" 
      required>

    @if ($errors->has('password_confirmation'))
      <p class="help is-danger">
        {{ $errors->first('password_confirmation') }}
      </p>
    @endif
  </div> <!-- field docente-password_confirmation -->

  <div class="field">
    <button class="button is-primary" type="submit" name="button">Registrar</button>
    <a class="help" href="{{ route('login') }}">
      Já tem conta? Acesse aqui.
    </a>
  </div> <!-- field button -->

  </div> <!-- uk-animation-fade -->

</div> <!-- form-docente -->

