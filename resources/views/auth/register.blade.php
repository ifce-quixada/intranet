@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">

    <div class="columns is-centered">
      <div class="column is-half">

        <div class="box">
          <h2 class="title has-text-centered">Registro de usuário</h2>
          <form class="" role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

        <div class="field">
          <div class="control is-expanded">
            <span class="select is-fullwidth">
            <select 
              id="user_type" 
              class="" name="user_type" required onchange="verifyUserType(this)">
                <option value="null">Escolha um tipo de usuário</option>
                <!-- <option value="docente">Docente</option> -->
                <option value="tae">Administrativo</option>
                <!--<option value="aluno">Aluno</option> -->
            </select>
            </span>
          </div> <!-- control -->
        </div> <!-- field -->

        <div id="wrap-docente">
          @include ('auth.form-docente', ['user' => null])       
        </div>

        <div id="wrap-administrativo">
          @include ('auth.form-administrativo', ['user' => null])       
        </div>
        
      </form>
      </div> <!-- box -->
      </div> <!-- uk-card -->
    </div> <!-- uk-width -->

  </div> <!-- uk-container -->
</div> <!-- uk-section -->

@endsection

@section('jscontent')
<script>
  var storageFormDocente = document.getElementById("wrap-docente").innerHTML;
  var storageFormAdministrativo = document.getElementById("wrap-administrativo").innerHTML;
  console.log(storageFormDocente);
  document.getElementById("form-docente").remove();
  document.getElementById("form-administrativo").remove();

  function removeRequiredAttr(element, index, array) {
    console.log(element);
    element.removeAttribute("required");
  }

  function setRequiredAttr(element, index, array) {
    element.setAttribute("required", true);
  }

  function verifyUserType(that) {

    if (that.value == "null") {
      document.getElementById("form-administrativo").setAttribute("hidden", true);
      document.getElementById("form-docente").setAttribute("hidden", true);

    } else if (that.value == "docente") {
      document.getElementById("wrap-docente").innerHTML = storageFormDocente;
      document.getElementById("form-docente").removeAttribute("hidden");
      document.getElementById("form-administrativo").remove();

    } else if (that.value == "tae") {
      document.getElementById("wrap-administrativo").innerHTML = storageFormAdministrativo;
      document.getElementById("form-administrativo").removeAttribute("hidden");
      document.getElementById("form-docente").remove();

    }
  }

  </script>
@endsection
