@extends('layouts.modules')

@section('content')
<div class="section">
  <div class="container">

    @if ($setting)
    <div class="box has-background-success is-shadowless">
    <div class="columns">
        <div class="column has-text-centered has-text-white">
            <p class="content is-marginless is-paddingless">Semestre atual</p>
            <p class="title has-text-white">{{ $setting->current_semester }}</p>
        </div>

        <div class="column has-text-centered has-text-white">
            <p class="content is-marginless is-paddingless">Primeiro dia letivo do semestre</p>
            <p class="title has-text-white">{{ $setting->first_school_day}}</p>
        </div>

        <div class="column has-text-centered has-text-white">
            <p class="content is-marginless is-paddingless">Último dia letivo do semestre</p>
            <p class="title has-text-white">{{ $setting->last_school_day}}</p>
        </div>
    </div>
    </div>
    @endif

    <h1 class="title has-text-centered">Sistemas Internos</h1>
    <div class="columns">

      <div class="column animation-toggle has-text-centered">
        <a class="" href="{{ route('branches.branch.index') }}">
        <div class="box">
          <span class="icon is-large">
            <i class="mdi mdi-48px mdi-phone"></i>
          </span>
          <h4>Ramais</h4>
        </div>
        </a>
      </div>

      <div class="column animation-toggle has-text-centered">
        <a class="" href="{{ route('informatives.informative.index') }}">
        <div class="box">
          <span class="icon is-large">
            <i class="mdi mdi-48px mdi-newspaper"></i>
          </span>
          <h4>Informativos</h4>
        </div>
        </a>
      </div>

      <div class="column animation-toggle has-text-centered">
        <a class="" href="{{ route('frequencies.frequency.index') }}">
        <div class="box">
          <span class="icon is-large">
            <i class="mdi mdi-48px mdi-calendar-range"></i>
          </span>
          <h4>Frequência</h4>
        </div>
        </a>
      </div>

      <div class="column animation-toggle has-text-centered">
        <a class="" href="{{ route('tickets.ticket.index') }}">
        <div class="box">
          <span class="icon is-large">
            <i class="mdi mdi-48px mdi-lifebuoy"></i>
          </span>
          <h4>Abrir chamado</h4>
        </div>
        </a>
      </div>

    </div> <!-- columns -->

    <div class="columns">
      <div class="column">
        @if (Session::has('status_message'))
          <div class="notification is-{!! session('type_message') !!}">
            <button class="delete"></button>
            {!! session('status_message') !!}
          </div>
        @endif
      </div> <!-- column -->
    </div> <!-- columns -->

  </div>
</div>

@if (! count($links) == 0)
<div class="section">
  <div class="container">
    <h1 class="title has-text-centered">Sistemas Externos</h1>
    <ul class="uk-list">
      @foreach ($links as $link)
        <li><a href="{{ $link->link }}" target="_blank">{{ $link->description }}</a></li>
      @endforeach
  </div> <!-- /uk-container -->
</div> <!-- /uk-section -->
@endif

@endsection



@section('jscontent')
  <script>
    document.addEventListener('DOMContentLoaded', () => {
      (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
        $notification = $delete.parentNode;
        $delete.addEventListener('click', () => {
          $notification.parentNode.removeChild($notification);
        });
      });
    });
  @if(Session::has('status_message'))
    UIkit.notification({
      message: '{!! session('status_message') !!}',
      status: '{!! session('type_message') !!}',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif
  </script>
@endsection

