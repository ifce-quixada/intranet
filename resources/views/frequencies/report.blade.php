@extends('layouts.frequency')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm; 
  } 
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  #info {
    margin-bottom: 0.4cm; 
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h4 {
    text-align: center;
    margin: 0;
  }
</style>
@endsection

@section('content')

  <div id="frequency-content">
  <div> 
    <div>

      <table width="100%" id="header" border="0">
        <tr>
          <td>
            <img src="{{ public_path() . '/img/logo-ifce.png' }}" alt="logo ifce" width="200" />
          </td>
          <td>
            <h2>CONTROLE DE FREQUÊNCIA - {{ strtoupper($data['month_name']) }}/{{ $data['year'] }}</h2>
            <h4>CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}</h4>
          </td>
        </tr>
      </table>


      <table width="100%" id="info" border="0" color="black" cellpadding="0px">
        <tr>
          <td align="left" colspan="3"><strong>SERVIDOR:</strong> {{ $data['name'] }}</td>
          <td align="left" colspan="2"><strong>SIAPE:</strong> {{ $data['username'] }}</td>
          </tr>
        <tr>
          <td align="left" colspan="3"><strong>CARGO:</strong> {{ $data['responsability'] }}
            @if ($data['role'] != '')
              ({{ $data['role'] }})
            @endif
          </td>
          <td align="left" colspan="3"><strong>LOTAÇÃO:</strong> {{ $data['sector'] }}</td>
        </tr>
        <tr>
          <td align="left" colspan="3"><strong>HORÁRIO:</strong> 
            {{ $data['work_schedule'] }}
          </td>
          <td align="left" colspan="2"><strong>JORNADA:</strong> {{ $data['work_load'] }} horas</td>
        </tr>
      </table>

      <table width="100%" id="days" border="1" color="black" cellspacing="20px" cellpadding="2px" style="border-collapse:collapse;border-spacing:0;"> 
        <tr>
          <th align="center" colspan="4">1º TURNO</th>
          <th align="center" rowspan="2">DIAS</th>
          <th align="center" colspan="4">2º TURNO</th>
          <th align="center" rowspan="2">BCO<br>DE<br>HORAS</th>
        </tr>
        <tr>
          <th align="center">ENTRADA</th>
          <th align="center">RUBRICA</th>
          <th align="center">SAÍDA</th>
          <th align="center">RUBRICA</th>
          <th align="center">ENTRADA</th>
          <th align="center">RUBRICA</th>
          <th align="center">SAÍDA</th>
          <th align="center">RUBRICA</th>
        </tr>
          @for ($d = 1; $d <= $dt->daysInMonth; $d++)
          <?php $dt->day = $d ?>

          @if ($dt->between($startVacation, $endVacation))
          <tr>
            <td align="center" colspan="4">FÉRIAS</td>
            <td align="center">{{ $dt->day }}</td>
            <td align="center" colspan="4">FÉRIAS</td>
            <td></td>
          </tr>
            @continue
            @if ($dt->day == $endVacation->day)
            <tr>
              <td align="center" colspan="4">FÉRIAS</td>
              <td align="center">{{ $dt->day }}</td>
              <td align="center" colspan="4">FÉRIAS</td>
              <td></td>
            </tr>
            @endif
            @continue
          @elseif ($dt->isWeekend())
            <tr>
              @if ($dt->dayOfWeek == 6)
                <td align="center" colspan="4">SÁBADO</td>
              @elseif ($dt->dayOfWeek == 0)
                <td align="center" colspan="4">DOMINGO</td>
              @endif
              <td align="center">{{ $dt->day }}</td>
              @if ($dt->dayOfWeek == 6)
                <td align="center" colspan="4">SÁBADO</td>
              @elseif ($dt->dayOfWeek == 0)
                <td align="center" colspan="4">DOMINGO</td>
              @endif
              <td></td>
            </tr>
            @continue

          @else
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td align="center">{{ $dt->day }}</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          @endif 
          @endfor
      </table>

      <table width="100%" id="footer-left" border="0" cellpadding="10px">
        <tr>
          <td colspan="4" width="8cm" style="border:1px solid black;"><strong>HORAS A COMPENSAR:</strong></td>
          <td width="4cm"></td>
          <td>VISTO DA CHEFIA IMEDIATA:</td>
        </tr>
      </table>    
      <table width="100%" id="footer-right" border="0" cellpadding="10px">
        <tr>
          <td colspan="4" width="8cm" style="border:1px solid black;"><strong>HORAS A DESCONTAR:</strong></td>
          <td width="4cm"></td>
          <td>VISTO DA CHEFIA IMEDIATA:</td>
        </tr>
      </table>

      <div class="page-break"></div>
      <h2 style="margin-bottom:10px;">OCORRÊNCIAS</h2>

      <table width="97%" id="ocurrencies" border="1" color="black" cellpadding="10px" style="border-collapse:collapse;"> 
        <tr>
          <th align="center">DIA</th>
          <th align="center">DESCRIÇÃO</th>
        </tr>
          @for ($d = 1; $d <= $dt->daysInMonth; $d++)
          <?php $dt->day = $d ?>
            @if ($dt->dayOfWeek == 6 || $dt->dayOfWeek == 0)
              @continue 
            @endif
            <tr>
              <td width="3cm"></td>
              <td></td>
            </tr>
          @endfor
      </table>

      <table width="100%" border="0">
        <tr>
          <td><strong>VISTO DA CHEFIA IMEDIATA:</strong></td>
        </tr>
      </table>

    </div> 
  </div> 
  </div>

@endsection

