@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
      <div class="box">
        <h2 class="title has-text-centered">Gerar frequência</h2>
    
        <form method="POST" action="{{ route('frequencies.frequency.store') }}" accept-charset="UTF-8" id="create_frequency_form" name="create_frequency_form" class="uk-form-stacked">
          {{ csrf_field() }}
          @include ('frequencies.form')

          <div class="control">
            <a class="button button-default" href="javascript:history.back()">Voltar</a>
            <input class="button is-primary" type="submit" value="Gerar">
          </div>
        </form>

      </div>
      </div> <!-- uk-card -->
    </div> <!-- uk-width -->
  </div> <!-- uk-container -->
</div> <!-- /uk-section -->
@endsection


@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

