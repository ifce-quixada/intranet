@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">
    <div class="uk-child-width-expand@s" uk-grid>
      <div class="uk-width-expand@m">

        <h4>Dashboard</h4>

        <div class="">
          <table class="uk-table uk-table-striped uk-table-small uk-table-divider uk-table-middle uk-table-hover">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Departamento</th>
                <th>CH total</th>
                <th>CH sala de aula</th>
                <th>CH ativ. compl.</th>
                <th>CH faltante</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($teachersUsers as $teacher)
              <tr>
                <td><a href="{{ route('managements.management.show', $teacher->id) }}">{{ $teacher->name }}</a></td>
                <td>{{ $teacher->meta['department'] }}</td>
                <td>{{ number_format($teacher->meta['workload'], 2, ',', '.') }} h</td>
                <td>{{ number_format($teacher->meta['workload_in_semester'], 2, ',', '.') }} h</td>
                <td>
                @if ($teacher->activities->sum('workload') == 0 && $teacher->meta['workload_in_semester'] < 20)
                  <span class="uk-text-danger">{{ number_format($teacher->activities->sum('workload'), 2, ',', '.') }} h</span>
                @else
                  {{ number_format($teacher->activities->sum('workload'), 2, ',', '.') }} h
                @endif
                </td>
                <?php $total = $teacher->meta['workload'] - (2 * $teacher->meta['workload_in_semester']) - $teacher->activities->sum('workload'); ?>
                <td>{{ number_format($total, 2, ',', '.') }} h</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div> <!-- class empty -->

        <div class="uk-width-1-1@m">
          <div class="uk-grid-collapse uk-child-width-1-2" uk-grid>

        <div>
          <div class="uk-card uk-card-small uk-card-default uk-card-body">
            <div class="chart-container" style="position:relative;">
              <h5 class="uk-text-uppercase uk-text-center">Atividades (por tipo)</h5>
              <canvas id="activities-bar-chart"></canvas>
            </div>
          </div>
        </div>

        <div>
          <div class="uk-card uk-card-small uk-card-default uk-card-body">
            <div class="chart-container" style="position:relative;">
              <h5 class="uk-text-uppercase uk-text-center">Usuários cadastrados (por tipo)</h5>
              <canvas id="users-pie-chart"></canvas>
            </div>
          </div>
        </div>
          </div> <!-- uk-grid -->
        </div> <!-- uk-width -->

        </div> <!-- class empty -->

        <div class="uk-width-1-3@m">
          <h4>Resumo</h4>
          <div class="uk-grid-collapse uk-child-width-1-2" uk-grid>

            <div>
              <div class="uk-card ii-card-primary uk-card-body uk-card-small">
                semestre:
                <h4>{{ $setting->current_semester }}</h4>
              </div>
            </div>

            <div>
              <div class="uk-card ii-card-secondary uk-card-body uk-card-small">
                <h4>{{ $setting->first_school_day }}</h4>
                início do semestre 
              </div>
            </div>

            <div>
              <div class="uk-card ii-card-secondary uk-card-body uk-card-small">
                <h4>{{ $numberOfTeachers }}</h4>
                professores cadastrados
              </div>
            </div>

            <div>
              <div class="uk-card ii-card-primary uk-card-body uk-card-small">
                <h4>{{ $numberOfActivities }}</h4>
                Atividades cadastradas 
              </div>
            </div>

          </div> <!-- uk-grid -->

        </div> <!-- class uk-width -->

        </div>

    </div>

@endsection

@section('jscontent')
  <script>
/*plotUsersPieChart();*/
  var usersPieChartElem = document.getElementById("users-pie-chart");
  var dataUsers = {
    datasets: [{
        data: [{!! json_encode($numberOfTeachers) !!}, {!! json_encode($numberOfTaes) !!}, {!! json_encode($numberOfStudents) !!}],
        backgroundColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
        ],
        borderWidth: 2,
    }],

    labels: [
        'Docentes',
        'TAEs',
        'Alunos'
    ]
  };
  var options = {
    title: {
      display: false,
    },
    animation: {
      animateScale: true
    },
    legend: {
      display: false
    }
  };

  var usersPieChart = new Chart(usersPieChartElem,{
    type: 'pie',
    data: dataUsers,
    options: options
  });

  var activitiesBarChartElem = document.getElementById("activities-bar-chart");
  var dataActivities = {
    labels: [
      "FIC", 
      "Manutenção ao Ensino", 
      "Apoio ao Ensino", 
      "Orientação", 
      "Ensino extracurricular", 
      "Pesquisa aplicada", 
      "Extensão", 
      "Gestão", 
      "Comissões ou Fiscalização"
    ],
    datasets: [{
      data: [
        {!! json_encode($nFICActivity) !!}, 
        {!! json_encode($nManutencaoActivity) !!}, 
        {!! json_encode($nApoioActivity) !!}, 
        {!! json_encode($nOrientacaoActivity) !!}, 
        {!! json_encode($nEnsinoActivity) !!}, 
        {!! json_encode($nPesquisaActivity) !!}, 
        {!! json_encode($nExtensaoActivity) !!}, 
        {!! json_encode($nGestaoActivity) !!}, 
        {!! json_encode($nComissoesActivity) !!}], 
      backgroundColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 106, 86, 1)',
        'rgba(5, 19, 132, 1)',
        'rgba(254, 2, 35, 1)',
        'rgba(15, 2, 55, 1)',
      ],
      borderWidth: 2
    }]
  };

  var options = {
    title: {
      display: false
    },
    legend: {
      display: false
    },
    animation: {
      animateScale: true
    }
  };

  var activitiesBarChart = new Chart(activitiesBarChartElem,{
    type: 'pie',
    data: dataActivities,
    options: options
  });


  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

