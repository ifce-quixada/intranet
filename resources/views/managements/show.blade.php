@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">

  <div class="uk-width-1-1@m uk-align-center">

  <div class="uk-card uk-card-body uk-card-secondary uk-card-small">
    <h1 class="uk-card-title">Prof. {{ $teacher->name }} <span class="uk-text-small">({{ $teacher->login }})</span></h1>

    <ul class="uk-list">
      <li>Referente ao semestre letivo: {{ $setting->current_semester }}</li>
      <li>Curso ou Departamento: {{ $teacher->meta['department'] }}</li>
      <li>Tipo de vínculo: {{ $teacher->meta['regime'] }}</li>
      <li>Regime de trabalho: {{ $teacher->meta['workload'] }}</li>
    </ul>
  </div>
  <div class="uk-card uk-card-body uk-card-small">
    <h3>Atividades</h3>
    <table class="uk-table uk-table-striped uk-table-small uk-table-divider">
      <thead>
        <tr>
          <th>Tipo</th>
          <th>Descrição</th>
          <th>Carga horária</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($activities as $activity)
          <tr>
            <td>{{ $activity->type }}</td>
            <td>{{ $activity->description }}</a></td>
            <td>{{ number_format($activity->workload, 2, ',', '.') }} h</td>
          </tr>
          @endforeach
            <tr>
              <td></td>
              <td></td>
              <td colspan="2">{{ number_format($activities->sum('workload'), 2, ',', '.') }} h no total</td>
            </tr>

          </tbody>
        </table>

      <a class="uk-button uk-button-default" href="javascript:history.back()">Voltar</a>
  </div>

</div>
</div>

@endsection
