@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
      <div class="box">
        <h2 class="title has-text-centered">Abrir chamado - Tecnologia da Informação</h2>
    
        <form method="POST" action="{{ route('tickets.ticket.send') }}" accept-charset="UTF-8" id="create_ticket_form" name="create_ticket_form" class="">
          {{ csrf_field() }}
          @include ('tickets.form-ti')
          <div class="control">
            <a class="button button-default" href="javascript:history.back()">Voltar</a>
            <input class="button is-primary" type="submit" value="Abrir">
          </div>
        </form>

      </div>
      </div> <!-- uk-card -->
    </div> <!-- uk-width -->
  </div> <!-- uk-container -->
</div> <!-- /uk-section -->
@endsection


@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

