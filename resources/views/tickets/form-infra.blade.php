  <div class="columns">
    <div class="column">

    <input id="queue" name="queue" class="input" type="hidden" value="infra">
    <p class="content">
      @if (!Auth::guest())
        <strong>Chamado aberto por:</strong><br />
        {{ Auth::user()->name }} ({{ Auth::user()->username }}) - {{ Auth::user()->sector }} <br />
        {{ Auth::user()->email }}
        <input name="name" class="input" type="hidden" value="{{ Auth::user()->name }}">
        <input name="login" class="input" type="hidden" value="{{ Auth::user()->login }}">
        <input name="email" class="input" type="hidden" value="{{ Auth::user()->email }}">
        <input name="sector" class="input" type="hidden" value="{{ Auth::user()->sector }}">

      @else
        <div class="field">
          <label class="label" for="name">Nome</label>
          <input id="name" name="name" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="login">SIAPE ou Matrícula</label>
          <input id="login" name="login" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="sector">Setor</label>
          <input id="sector" name="sector" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="email">E-mail</label>
          <input id="email" name="email" class="input" type="email" value="">
        </div>
      @endif
    </p>

    <div class="field">
      <label class="label" for="chief">Chefe imediato</label>
      <input id="chief" name="chief" class="input" type="text" value="">
    </div>

    <input id="emailto" name="emailto" class="input" type="hidden" value="osinfra.qxd@ifce.edu.br ">
    <div class="field">
      <div class="control is-expanded">
        <label class="label" for="subject">Assunto</label>
        <span class="select is-fullwidth">
        <select id="subject" name="subject" class="">
            <option selected disabled>Selecione uma opção</option>
            <optgroup label="Elétrica">
              <option value="Elétrica/Acompanhamento de Técnico Especialista">Acompanhamento de Técnico Especialista</option>
              <option value="Elétrica/Conserto de ar condicionado">Conserto de ar condicionado</option>
              <option value="Elétrica/Conserto de tomadas">Conserto de tomadas</option>
              <option value="Elétrica/Dreno de ar condicionado">Dreno de ar condicionado</option>
              <option value="Elétrica/Estrutura elétrica">Estrutura elétrica</option>
              <option value="Elétrica/Fixação de refletores">Fixação de refletores</option>
              <option value="Elétrica/Instalação de ar condicionado">Instalação de ar condicionado</option>
              <option value="Elétrica/Instalação de exaustor">Instalação de exaustor</option>
              <option value="Elétrica/Instalação de para raios">Instalação de para raios</option>
              <option value="Elétrica/Limpeza de ar condicionado">Limpeza de ar condicionado</option>
              <option value="Elétrica/Substituição de equipamento">Substituição de equipamento</option>
              <option value="Elétrica/Testar equipamento">Substituição de equipamento</option>
              <option value="Elétrica/Troca de lâmpadas">Troca de lâmpadas</option>
              <option value="Elétrica/Verificação de ar condicionado">Verificação de ar condicionado</option>
              <option value="Elétrica/Verificação de quadros elétricos">Verificação de quadros elétricos</option>
              <option value="Elétrica/Verificação de rotina">Verificação de rotina</option>
              <option value="Elétrica/Verificação de estrutura elétrica">Verificação de estrutura elétrica</option>
              <option value="Elétrica/Verificação de motor">Verificação de motor</option>
            </optgroup>
            <optgroup label="Estrutura Predial">
              <option value="Estrutura Predial/Alvenaria">Alvenaria</option>
              <option value="Estrutura Predial/Aplicação de silicone">Aplicação de silicone</option>
              <option value="Estrutura Predial/Bomba">Bomba</option>
              <option value="Estrutura Predial/Calçamento">Calçamento</option>
              <option value="Estrutura Predial/Captação de água pluvial">Captação de água pluvial</option>
              <option value="Estrutura Predial/Cercamento">Cercamento</option>
              <option value="Estrutura Predial/Consertar infiltração">Consertar infiltração</option>
              <option value="Estrutura Predial/Consertar porta">Consertar porta</option>
              <option value="Estrutura Predial/Consertar rachadura">Consertar rachadura</option>
              <option value="Estrutura Predial/Consertar forro/telhado">Consertar forro/telhado</option>
              <option value="Estrutura Predial/Instalação de objetos">Instalação de objetos</option>
              <option value="Estrutura Predial/Instalação ou fixação de objetos">Instalação ou fixação de objetos</option>
              <option value="Estrutura Predial/Limpeza">Limpeza</option>
              <option value="Estrutura Predial/Pintura">Pintura</option>
              <option value="Estrutura Predial/Reforma">Reforma</option>
              <option value="Estrutura Predial/Reforma/reboco/infiltração">Reforma/reboco/infiltração</option>
              <option value="Estrutura Predial/Reparar infiltração">Reparar infiltração</option>
              <option value="Estrutura Predial/Reparar vedação">Reparar vedação</option>
              <option value="Estrutura Predial/Reparar descarga">Reparar descarga</option>
              <option value="Estrutura Predial/Reparos">Reparos</option>
              <option value="Estrutura Predial/Trocar fechaduras">Trocar fechaduras</option>
              <option value="Estrutura Predial/Trocar porta">Trocar porta</option>
              <option value="Estrutura Predial/Verificar porta">Verificar porta</option>
            </optgroup>
            <optgroup label="Hidráulica">
              <option value="Hidráulica/Consertar caixa de descarga">Consertar caixa de descarga</option>
              <option value="Hidráulica/Consertar chuveiro">Consertar chuveiro</option>
              <option value="Hidráulica/Consertar hidrante">Consertar hidrante</option>
              <option value="Hidráulica/Consertar vazamento">Consertar vazamento</option>
              <option value="Hidráulica/Desentupimento de canos">Desentupimento de canos</option>
              <option value="Hidráulica/Drenagem">Drenagem</option>
              <option value="Hidráulica/Esgoto">Esgoto</option>
              <option value="Hidráulica/Instalação de chuveiros">Instalação de chuveiros</option>
              <option value="Hidráulica/Recuperar poço">Recuperar poço</option>
              <option value="Hidráulica/Retirar bomba">Retirar bomba</option>
              <option value="Hidráulica/Verificar caixa d'água">Verificar caixa d'água</option>
            </optgroup>
            <optgroup label="Jardinagem">
              <option value="Jardinagem/Capinagem">Capinagem</option>
              <option value="Jardinagem/Dedetização">Dedetização</option>
              <option value="Jardinagem/Plantio">Plantio</option>
              <option value="Jardinagem/Podar">Podar</option>
              <option value="Jardinagem/Serviço de capinagem">Serviço de capinagem</option>
            </optgroup>
            <optgroup label="Limpeza">
              <option value="Limpeza/Resíduos Sólidos">Resíduos Sólidos</option>
            </optgroup>
            <optgroup label="Mudança">
              <option value="Mudança/Transferência temporária">Transferência temporária</option>
              <option value="Mudança/Transferir livros">Transferir livros</option>
              <option value="Mudança/Transferir mobiliário">Tranferir mobilária</option>
              <option value="Mudança/Transporte de material expediente">Transporte de material expediente</option>
            </optgroup>
            <optgroup label="Rede lógica">
              <option value="Rede lógica/Ponto de rede de dados">Ponto de rede de dados</option>
            </optgroup>
            <optgroup label="Serviços Adicionais">
              <option value="Serviços Adicionais/Apoio de pessoal">Apoio de pessoal</option>
              <option value="Serviços Adicionais/Empréstimo temporário">Empréstimo temporário</option>
              <option value="Serviços Adicionais/Fazer café ou chá">Fazer café ou chá</option>
              <option value="Serviços Adicionais/Montagem de estrutura elétrica">Montagem de estrutura elétrica</option>
            </optgroup>
        </select>
        </span>
      </div>
    </div>

    <div class="field">
      <label class="label" for="content">Descreva o problema/requisição aqui</label>
      <div class="control">
        <textarea id="content" name="content" class="textarea" placeholder=""></textarea>
      </div>
    </div>


    </div> <!-- column -->
  </div> <!-- columns -->

@section('jscontent')
<script>
</script>
@endsection
