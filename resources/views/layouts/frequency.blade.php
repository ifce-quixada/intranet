<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet"> -->
    @yield('meta')
</head>

<body>
    <div id="frequency">
        @yield('content')
    </div>

    <!-- Scripts -->
</body>

</html>
