<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Intranet Campus {{ \Illuminate\Support\Str::title(config('app.campus', '')) }}</title>

    <!-- favicon icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"> -->
    <!-- <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png"> -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"> -->
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
    <div class="section">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column">
            <div class="">
              <!-- Branding Image -->
              <a class="logo" href="{{ url('/') }}">
                <img src="{{ URL::asset('/img/logo-ifce.png') }}" alt="Intranet Campus {{ config('app.name', '') }}" class="if-logo" />
              </a>
            </div> <!-- -->
          </div> <!-- column -->

          <div class="column">
            @include('layouts.navigation')
          </div> <!-- column -->

        </div> <!-- columns -->
      </div> <!-- container -->

      @yield('content')
    </div> <!-- section -->

    <div class="section">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column">
        <div class="notification is-paddingless is-white has-text-centered">
          <ul class="is-size-7">
            <li>Desenvolvido por CTI - Quixadá</li>
            <li>Precisando de ajuda? <a class="modal-button" data-target="modal-support">Clique aqui</a></li>
            <!-- <li>Instruções para implantação no seu campus</li> -->
          </ul>
        <div>
      </div>
        <div>
      </div>
    </div>

    <!-- This is the modal -->
    <div id="modal-support" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="box">
        <h2 class="title">Suporte</h2>
        <p class="content">
           Esse sistema foi desenvolvido pela Coordenação de Tecnologia da Informação do
           IFCE - Campus Quixadá. Caso você tenha alguma dúvida,
           <a href="{{ route('tickets.ticket.index') }}">abra um chamado</a>,
           ou ligue para o telefone (85) 3455-3025  - ramal 2219.
        </p>
        <p class="content has-text-right">
          <a class="button is-primary" data-target="modal-support">Entendi.</a>
        </p>
      </div> <!-- box -->
      </div> <!-- modal-content -->
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('jscontent')

    </div> <!-- section -->
  </div> <!-- app -->
</body>
</html>
