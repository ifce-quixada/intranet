@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="">
      <div class="box">

        <div id="notification" class="notification is-success is-light is-hidden">
            {{ session('success_message') }}
        </div>

        <h2 class="title">Configurações</h2>
        <p><a class="button is-info" href="{{ route('admin.configuracoes.create') }}">Adicionar nova informação</a></p>

        <table class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Semestre atual</th>
              <th>Primeiro dia de aula</th>
              <th>Último dia de aula</th>
              <th>Ações</th>
            </tr>
          </thead>

          <tbody>
          @foreach ($settings as $setting)
            <tr>
              <td>{{ $setting->current_semester }}</td>
              <td>{{ $setting->first_school_day }}</td>
              <td>{{ $setting->last_school_day }}</td>
              <td>
                <a href="{{ route('admin.configuracoes.edit', $setting->id ) }}" class="button is-white is-text" >
                  <span class="icon">
                    <i class="mdi mdi-pencil"></i>
                  </span>
                </a>
                <form id="setting-form" class="is-pulled-left" name="deleteSettingForm" method="POST" action="{!! route('admin.configuracoes.destroy', $setting) !!}" accept-charset="UTF-8">
                    <input name="_method" value="DELETE" type="hidden">
                    {{ csrf_field() }}
                    <button class="button is-white is-text" type="submit">
                        <span class="icon">
                            <i class="mdi mdi-delete"></i>
                        </span>
                    </button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    var notification = document.getElementById('notification');
    notification.classList.remove('is-hidden');
    setTimeout(function(){ notification.classList.add('is-hidden'); }, 5000);
  @endif
  </script>
@endsection


