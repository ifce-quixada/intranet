@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
      <div class="box">
        <h2 class="title">Editar informações</h2>
        @if ($errors->any())
        <div class="notification">
          <h4 class="title">Verifique se o formulário está correto</h4>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

        <form method="POST" action="{{ route('admin.configuracoes.update', $setting->id) }}" id="edit_setting_form" name="edit_setting_form" accept-charset="UTF-8" class="uk-form">
          {{ csrf_field() }}
          <input name="_method" type="hidden" value="PUT">
          @include ('admin.settings.form', ['setting' => $setting])

          <div class="control">
            <a class="button button-default" href="javascript:history.back()">Voltar</a>
            <input class="button is-primary" type="submit" value="Atualizar">
          </div>
        </form>

      </div>
      </div>
    </div>
  </div>
</div>

@endsection
