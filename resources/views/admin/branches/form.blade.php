<div class="columns">
  <div class="column">

    <div class="field">
      <label class="label" for="sector">Setor</label>
      <div class="control">
        <input class="input {{ $errors->has('sector') ? 'is-danger' : '' }}" name="sector" type="text" id="sector" value="{{ old('sector', optional($branch)->sector) }}" minlength="1" maxlength="30" required="true" placeholder="Nome do setor">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

    <div class="field">
      <label class="label" for="contacts">Contatos</label>
      <div class="control">
        <input class="input {{ $errors->has('contacts') ? 'is-danger' : '' }}" name="contacts" type="text" id="contacts" value="{{ old('contacts', optional($branch)->contacts) }}" minlength="10" maxlength="30" required="true" placeholder="Contatos">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

    <div class="field">
      <label class="label" for="branch_line">Ramal</label>
      <div class="control">
        <input class="input {{ $errors->has('branch_line') ? 'is-danger' : '' }}" name="branch_line" type="text" id="branch_line" value="{{ old('branch_line', optional($branch)->branch_line) }}" minlength="1" maxlength="4" required="true" placeholder="Ramal">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

  </div>
</div>
