@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="">
      <div class="box">

        <div id="notification" class="notification is-success is-light is-hidden">
            {{ session('success_message') }}
        </div>

        <h2 class="title">Ramais</h2>
        <p><a class="button is-info" href="{{ route('admin.ramais.create') }}">Adicionar novo ramal</a></p>

        <table class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Setor</th>
              <th>Contatos</th>
              <th>Ramal</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($branches as $branch)
            <tr>
              <td>{{ $branch->sector }}</td>
              <td>{{ $branch->contacts }}</td>
              <td>{{ $branch->branch_line }}</td>
              <td>
                <a href="{{ route('admin.ramais.edit', $branch ) }}" class="button is-white is-text">
                  <span class="icon">
                    <i class="mdi mdi-pencil"></i>
                  </span>
                </a>

                <form id="branch-form" class="is-pulled-left" name="deleteBranchForm" method="POST" action="{!! route('admin.ramais.destroy', $branch) !!}" accept-charset="UTF-8">
                	<input name="_method" value="DELETE" type="hidden">
                  	{{ csrf_field() }}
                    <button class="button is-white is-text" type="submit">
                        <span class="icon">
                            <i class="mdi mdi-delete"></i>
                        </span>
                    </button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>

      </div>
    </div>
  </div> <!-- container -->
</div> <!-- /section -->
@endsection

@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    var notification = document.getElementById('notification');
    notification.classList.remove('is-hidden');
    setTimeout(function(){ notification.classList.add('is-hidden'); }, 5000);
  @endif
  </script>
@endsection

