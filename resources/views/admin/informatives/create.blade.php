@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
      <div class="box">
        <h2 class="title">Cadastrar novo informativo</h2>
        @if ($errors->any())
        <div class="notification">
          <h4 class="title">Verifique se o formulário está correto</h4>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

        <form method="POST" action="{{ route('admin.informativos.store') }}" accept-charset="UTF-8" id="create_informative_form" name="create_informative_form">
          {{ csrf_field() }}
          @include ('admin.informatives.form', ['informative' => null])

          <div class="control">
            <a class="button button-default" href="javascript:history.back()">Voltar</a>
            <input class="button is-primary" type="submit" value="Adicionar">
          </div>

        </form>

      </div>
      </div>
    </div>
  </div>
</div>

@endsection

