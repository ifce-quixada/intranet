@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="">
      <div class="box">

        <div id="notification" class="notification is-success is-light is-hidden">
            {{ session('success_message') }}
        </div>

        <h2 class="title">Informativos</h2>
        <p><a class="button is-info" href="{{ route('admin.informativos.create') }}">Adicionar novo informativo</a></p>

        <table class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Data da publicação</th>
              <th>Título</th>
              <th>Link</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($informatives as $informative)
            <tr>

              <td>{{ date('d/m/Y', strtotime($informative->published_at)) }}</td>
              <td>{{ $informative->title }}</td>
              <td><a href="{{ $informative->link }}">{{ $informative->link }}</a></td>
              <td>
                <a href="{{ route('admin.informativos.edit', $informative ) }}" class="button is-white is-text">
                  <span class="icon">
                    <i class="mdi mdi-pencil"></i>
                  </span>
                </a>

                <form id="informative-form" class="is-pulled-left" name="deleteInformativeForm" method="POST" action="{!! route('admin.informativos.destroy', $informative) !!}" accept-charset="UTF-8">
                    <input name="_method" value="DELETE" type="hidden">
                    {{ csrf_field() }}
                    <button class="button is-white is-text" type="submit">
                        <span class="icon">
                            <i class="mdi mdi-delete"></i>
                        </span>
                    </button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>


      </div>
    </div>
  </div> <!-- container -->
</div> <!-- /section -->
@endsection

@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    var notification = document.getElementById('notification');
    notification.classList.remove('is-hidden');
    setTimeout(function(){ notification.classList.add('is-hidden'); }, 5000);
  @endif
  </script>
@endsection

