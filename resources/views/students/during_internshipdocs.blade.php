@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-2 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        RELATÓRIO DIÁRIO DE ATIVIDADES<br /> <br />
    </h3>


    <table width="100%" id="relatorio-atividades" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong>Nome do estagiário:</strong> {{ $data['studentName'] }}</td>
        </tr>
        <tr>
            <td align="left" colspan="3"><strong>Nome da empresa:</strong> {{ $data['companyName'] }}</td>
        </tr>
        <tr>
            <td align="left" colspan="3"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Professor Orientador do IFCE:</strong> {{ $data['advisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Supervisor da Empresa:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Mês:</strong> {{ $data['activityMonth'] }}</td>
        </tr>
        <tr>
            <td align="left" colspan="3"><strong>Ano:</strong> {{ $data['activityYear'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Total de horas do mês:</strong> {{ $data['activityHoursMonth'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Total de horas acumuladas:</strong> {{ $data['activityTotalHours'] }}</td>
        </tr>
    </table> <!-- relatorio-atividades -->

    <br /> <br />

    <table width="90%" id="horas-trabalhadas" color="black" cellpadding="0px" align="center">
        <tr>
            <td align="center">
                <i>DATA E HORAS TRABALHADAS</i>
            </td>
            <td align="center"><i>ATIVIDADES</i></td>
            <td align="center">
                <i>OBSERVAÇÕES</i>
            </td>
        </tr>
        @for ($e = 0; $e <= (count($data['activities'])/2); $e++)
            <tr>
                <td align="center" style="padding:2px;">{{ $data['activities'][$e]['date'] }}</td>
                <td style="padding:2px;">{{ $data['activities'][$e]['content'] }}</td>
                <td align="center" style="padding:2px;">{{ $data['activities'][$e]['obs'] }}</td>
            </tr>
        @endfor
    </table> <!-- horas-trabalhadas -->

    <!-- nova pagina -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <table width="90%" id="horas-trabalhadas-pag-2" color="black" cellpadding="0px" align="center">
        <tr>
            <td align="center">
                <i>DATA E HORAS TRABALHADAS</i>
            </td>
            <td align="center"><i>ATIVIDADES</i></td>
            <td align="center"><i>OBSERVAÇÕES</i></td>
        </tr>

        @for ($e = (count($data['activities'])/2) + 1; $e < count($data['activities']); $e++)
            <tr>
                <td align="center" style="padding:2px;">{{ $data['activities'][$e]['date'] }}</td>
                <td style="padding:2px;">{{ $data['activities'][$e]['content'] }}</td>
                <td align="center" style="padding:2px;">{{ $data['activities'][$e]['obs'] }}</td>
            </tr>
        @endfor
    </table> <!-- horas-trabalhadas-pag-2 -->

    <br /> <br /> <br /> <br />

    <div class="espaco-reservado-1">
        <p>
            <i>Notas:</i><br/>
            <i>Máquinas, aparelhos, equipamentos, instrumentos utilizados.</i>
        </p>
        <div style="border-bottom: 1px solid #000;height:12px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>

        <table align="center">
            <tr>
                <td>
                    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:80px auto 0 auto;">
                    <strong>Assinatura e carimbo do professor do IFCE</strong>
                </td>

                <td>
                    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:80px auto 0 auto;">
                    <strong>Assinatura e carimbo do supervisor da empresa</strong>
                </td>
            </tr>
        </table>
    </div> <!-- espaco-reservado-1 -->
    <!-- fim anexo-2 -->

    <!-- anexo-3 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        RELATÓRIO PERIÓDICOS DE ATIVIDADES<br /> <br />
    </h3>

    <table width="100%" id="relatorio-periodico-atividades" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />I) IDENTIFICAÇÃO DA EMPRESA</strong></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Nome da empresa:</strong> {{ $data['companyName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Supervisor da Empresa:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />II) IDENTIFICAÇÃO DO(A) ESTAGIÁRIO(A)</strong></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Nome</strong> {{ $data['studentName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Matrícula:</strong> {{ $data['studentRegistration'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Período avaliado:</strong> {{ $data['activityMonth'] }}/{{ $data['activityYear'] }}</td>
        </tr>
        <tr>
            <td align="left" colspan="3"><strong>Professor(a) orientador(a):</strong> {{ $data['advisorName'] }}</td>
        </tr>
    </table> <!-- relatorio periodico atividades -->

    <div class="atividades-desenvolvidas-durante-estagio">
        <p style="text-align:center;">
            <strong>III) ATIVIDADES DESENVOLVIDAS DURANTE O ESTÁGIO<br> NO PERÍODO DE {{ $data['activityPeriod'] }}</strong>
        </p>
        <p>
            {{ $data['developedActivities'] }}
        </p>
    </div> <!-- atividades-desenvolvidas-durante-estagio -->

    <div class="observacoes-ou-comentarios">
        <p style="text-align:center;">
            <strong>IV) OBSERVAÇÕES OU COMENTÁRIOS</strong>
        </p>
        <p>
            {{ $data['activitiesObservations'] }}
        </p>
    </div> <!-- observacoes ou comentarios -->

    <!-- nova-pagina -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <div class="avaliacao-do-estagiario">
        <p style="text-align:center;">
            <strong>IV) AVALIAÇÃO DO(A) ESTAGIÁRIO(A)</strong>
        </p>
        <ul>
            <li>
                Conceitos:<br/>
                (4) Muito Satisfatório<br/>
                (3) Satisfatório<br/>
                (2) Pouco Satisfatório<br/>
                (1) Insatisfatório<br/>
            </li>
        </ul>
        <ul>
            <li>
                Critérios:<br/>
                ({{ $data['avaliationTheoricKnowledge'] }}) Aplicação do conhecimento teórico<br/>
                ({{ $data['avaliationRelationship'] }}) Relacionamento<br/>
                ({{ $data['avaliationPontuality'] }}) Assiduidade/pontualidade<br/>
                ({{ $data['avaliationLearning'] }}) Aprendizado<br/>
                ({{ $data['avaliationInitiative'] }}) Iniciativa<br/>
                ({{ $data['avaliationCooperation'] }}) Cooperação<br/>
            </li>
        </ul>

        <div style="text-align:center; margin:40px auto 0 auto;">
            _______________________,_____ de ___________________ de _______
        </div>

    <br /> <br /> <br /> <br />

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) estagiario(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) professor(a) orientador(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) supervisor(a) da parte concedente de estágio</strong>
    </div>
    <!-- fim anexo-3 -->

</div> <!-- internshipdocs-content -->

@endsection
