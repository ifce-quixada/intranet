@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-7 pagina 1 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        RELATÓRIO FINAL DE ATIVIDADES<br /> <br />
    </h3>

    <table width="100%" id="relatorio-final-atividades-cursos-superior" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="2"><strong>Nome:</strong> {{ $data['studentName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Campus:</strong> {{ ucwords(mb_strtolower(config('app.campus', 'Fortaleza'), "UTF-8")) }}</td>
        </tr>
        <tr>
            <td align="left" colspan="1"><strong>Parte concedente:</strong> {{ $data['companyName'] }}</td>
        </tr>
        <tr>
            <td align="left" colspan="1"><strong>Supervisor do estágio:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

    </table> <!-- relatorio-final-atividades-cursos-superior -->

    <table width="100%" id="relatorio-final-atividades-cursos-superior-2" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left"><strong>Período de realização:</strong> {{ $data['internshipPeriod'] }}</td>
        </tr>
        <tr>
            <td align="left"><strong>Setor de estágio na parte concedente:</strong> {{ $data['internshipSector'] }}</td>
        </tr>

        <tr>
            <td align="left"><strong>Total de horas acumuladas no estágio:</strong> {{ $data['internshipTotalHours'] }}</td>
        </tr>
    </table> <!-- relatorio-final-atividades-cursos-superior-2 -->

    <p>
        <strong>Descrever as atividades realizadas durante período de estágio: </strong><br />
        {{ $data['internshipCompanyActivities'] }}
    </p>

    <p>
        <strong>Descrever as dificuldades encontradas no estágio: </strong><br />
        {{ $data['internshipDificulties'] }}
    </p>

    <p>
        <strong>Faça um paralelo entre o aprendizado recebido no Instituto Federal do Ceará e a experiência vivenciada  no estágio: </strong><br />
        {{ $data['internshipCompare'] }}
    </p>

    <!-- anexo-4 pagina-2 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <p>
        <strong>Faça uma avaliação do nível de desenvolvimento das habilidades abaixo que o estágio lhe proporcionou:</strong><br />
    </p>

    <table width="80%" id="desempenho-funcional-estagiario" border="0" color="black" cellpadding="0px" style="margin:0 auto;">
        <tr>
            <td></td>
            <td style="text-align:center;">Ótimo</td>
            <td style="text-align:center;">Bom</td>
            <td style="text-align:center;">Regular</td>
            <td style="text-align:center;">Insuficiente</td>
        </tr>
        <tr>
            <td>a) Conhecimentos Gerais e Técnicos</td>
            <td style="text-align:center;">( {{ $data['scoreGeneralKnowledge'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreGeneralKnowledge'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreGeneralKnowledge'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreGeneralKnowledge'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>b) Iniciativa</td>
            <td style="text-align:center;">( {{ $data['scoreInitative'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInitative'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInitative'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInitative'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>c) Criatividade</td>
            <td style="text-align:center;">( {{ $data['scoreCriativity'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCriativity'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCriativity'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCriativity'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>d) Discrição (Ética)</td>
            <td style="text-align:center;">( {{ $data['scoreEthics'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEthics'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEthics'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEthics'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>e) Organização e método de trabalho</td>
            <td style="text-align:center;">( {{ $data['scoreOrganization'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreOrganization'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreOrganization'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreOrganization'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>f) Sociabilidade e Desempenho</td>
            <td style="text-align:center;">( {{ $data['scoreSocial'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSocial'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSocial'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSocial'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>g) Cooperação</td>
            <td style="text-align:center;">( {{ $data['scoreCooperation'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCooperation'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCooperation'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreCooperation'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>
    <!-- </table> desempenho funcionado do estagiario -->

    <!-- <table width="80%" id="desempenho-funcional-estagiario-parte-2" border="0" color="black" cellpadding="0px" style="margin:0 auto;"> -->
        <tr>
            <td>h) Liderança</td>
            <td style="text-align:center;">( {{ $data['scoreLeadership'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLeadership'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLeadership'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLeadership'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>i) Assiduidade e Pontualidade</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>j) Responsabilidade</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsibility'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsibility'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsibility'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsibility'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>l) Integração</td>
            <td style="text-align:center;">( {{ $data['scoreIntegration'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreIntegration'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreIntegration'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreIntegration'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>m) Envolvimento nas tarefas</td>
            <td style="text-align:center;">( {{ $data['scoreEngagement'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEngagement'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEngagement'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreEngagement'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>n) Capacidade de gerenciamento</td>
            <td style="text-align:center;">( {{ $data['scoreManagement'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreManagement'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreManagement'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreManagement'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>
    </table> <!-- desempenho funcional do estagiario - parte 2 -->

    <p>
        <strong>Considerações finais:</strong><br />
        {{ $data['internshipFinalConsiderations'] }}
    </p>

    <div style="text-align:center; margin:40px auto 0 auto;">
        _______________________,_____ de ___________________ de _______
    </div>

    <br /> <br /> <br /> <br />

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) estagiario(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) professor(a) orientador(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) supervisor(a) da parte concedente de estágio</strong>
    </div>

</div> <!-- internshipdocs-content -->
@endsection
