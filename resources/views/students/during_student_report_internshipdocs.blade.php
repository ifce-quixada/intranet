@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-2 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        RELATÓRIO MENSAL DE ATIVIDADES<br /> <br />
    </h3>


    <table width="100%" id="relatorio-atividades" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="2"><strong>Nome do estagiário:</strong> {{ $data['studentName'] }}</td>
            <td align="left" colspan="2"><strong>Matrícula:</strong> {{ $data['studentRegistration'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
            <td align="left" colspan="2"><strong>Telefone:</strong> {{ $data['studentPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="4"><strong>Nome da empresa:</strong> {{ $data['companyName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Professor Orientador do IFCE:</strong> {{ $data['advisorName'] }}</td>
            <td align="left" colspan="2"><strong>Supervisor da Empresa:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

    </table> <!-- relatorio-atividades -->

    <br />

    <table width="100%" id="relatorio-atividades-2" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="4"><strong>Ano/Mês:</strong> {{ $data['internshipReportDate'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Número de dias estagiados no mês:</strong> {{ $data['internshipDaysPerMonth'] }}</td>
            <td align="left" colspan="1"><strong>Horas estagiadas por dia:</strong> {{ $data['internshipHoursPerDay'] }}</td>
            <td align="left" colspan="1"><strong>Número de horas estagiadas no mês:</strong> {{ $data['internshipHoursPerMonth'] }}</td>
            <td align="left" colspan="1"><strong>Número de horas acumuladas no estágio:</strong> {{ $data['internshipTotalHours'] }}</td>
        </tr>

    </table> <!-- relatorio-atividades-2 -->

    <br />

    <div class="atividades-desenvolvidas-durante-estagio">
        <p>
            <b>
                Principais atividades desempenhadas no estágio:
                <!--
                Descreva as principais atividades desempenhadas no estágio
                durante o mês, ressaltando as dificuldades encontradas e soluções adotadas.
                Informe os equipamentos, máquinas e instrumentos utilizados.  As atividades
                deverão estar de acordo com aquelas previstas inicialmente no Termo de
                Compromisso do Estágio
                -->
            </b>
        </p>
        <p>
            {{ $data['internshipActivities'] }}
        </p>
    </div> <!-- atividades-desenvolvidas-durante-estagio -->

    <div style="text-align:center; margin:40px auto 0 auto;">
        _______________________,_____ de ___________________ de _______
    </div>

    <br /> <br /> <br /> <br />

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) estagiario(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) professor(a) orientador(a)</strong>
    </div>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) supervisor(a) da parte concedente de estágio</strong>
    </div>
    <!-- fim anexo-3 -->

</div> <!-- internshipdocs-content -->

@endsection
