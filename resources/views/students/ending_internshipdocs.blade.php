@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-4 pagina 1 -->
    @if ($data['studentCourseType'] == 'Técnico')
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        RELATÓRIO FINAL DE ESTÁGIO PARA CURSOS TÉCNICOS<br /> <br />
    </h3>

    <table width="100%" id="relatorio-final-atividades-cursos-tecnicos" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />ESTAGIÁRIO(A)</strong><br /></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Nome</strong> {{ $data['studentName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />EMPRESA</strong><br /></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Nome da empresa:</strong> {{ $data['companyName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Supervisor da Empresa:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />ESTÁGIO</strong><br /></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Período de realização do estágio:</strong> {{ $data['internshipPeriod'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Setor de estágio na parte concedente:</strong> {{ $data['internshipSector'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3">
                <strong>Situação:</strong> {{ $data['studentSituation'] }}
                @if ( $data['studentSituation'] == "Outro")
                ({{ $data['studentOtherSituation'] }})
                @endif
            </td>
        </tr>

        @if ( $data['studentSituation'] == "Empregado")
        <tr>
            <td align="left" colspan="3"><strong>O emprego foi decorrente do estágio?</strong> {{ $data['studentJobSituation'] }}</td>
        </tr>
        @endif

        <tr>
            <td align="left" colspan="3"><strong>Total de horas do estágio:</strong> {{ $data['internshipTotalHours'] }}h</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>1. Obtenção do estágio:</strong> {{ $data['internshipPlacement'] }}</td>
        </tr>
    </table> <!-- relatorio-final-atividades-cursos-tecnicos  -->

    <p>
        <strong> 2. Descreva as atividades desenvolvidas pela parte concedente do estágio: </strong><br />
        {{ $data['internshipCompanyActivities'] }}
    </p>
    <p>
        <strong> 3. Enumere as atividades realizadas durante o seu período de estágio: </strong><br />
        {{ $data['internshipDevelopedActivities'] }}
    </p>

    <!-- anexo-4 pagina-2 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <p>
        <strong>
        4. Especifique as principais dificuldades encontradas durante seu estágio, estabelecendo uma
        comparação entre os conhecimentos técnicos na instituição de ensino e as atividades práticas
        desenvolvidas na parte concedente do estágio.
        </strong><br />
        {{ $data['internshipDificulties'] }}
    </p>

    <p>
        <strong>
        5.  A empresa oferece estrutura adequada para a realização do estágio?
        </strong><br />
        {{ $data['internshipCompanyQualities'] }}

    </p>

    <p>
        <strong>
        6. Conhecimentos adquiridos no estágio. Cite treinamentos, cursos, seminários, leituras de
        manuais, livros técnicos, etc:
        </strong><br />
        {{ $data['internshipAcquiredSkills'] }}
    </p>

    <p>
        <strong>7. Avalie:</strong><br />
    </p>

    <p style="margin-left:20px;">
        <strong>a) A assistência e orientação dada pela parte concedente para execução de suas atividades
        durante o estágio:
        </strong><br />
        {{ $data['internshipCompanyAssistance'] }}
    </p>

    <p style="margin-left:20px;">
        <strong>b) O acompanhamento de seu estágio pelo IFCE:</strong><br />
        {{ $data['internshipInstitutionAssistance'] }}
    </p>

    <p>
        <strong>8. Conclusão:</strong><br />
        <strong>8.1 Dê sua opnião sobre:</strong>
    </p>

    <p style="margin-left:20px;">
        <strong>
        a) Sua participação como estagiário da parte concedente:
        </strong><br />
        {{ $data['internshipSelfAvaliationAsIntern'] }}
    </p>

    <p style="margin-left:20px;">
        <strong>
        b) Como se sente frente ao mercado de trabalho:
        </strong><br />
        {{ $data['internshipSelfAvaliationAsEmployee'] }}
    </p>

    <!-- anexo-4 pagina-3 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <p>
        <strong>8.2 Críticas e sugestões:</strong>
    </p>

    <p style="margin-left:20px;">
        <strong>Indique que assunto deve ser incluído ou excluído no seu curso. Justifique.</strong><br />
        {{ $data['internshipSubjectSuggestion'] }}
    </p>

    <p style="margin-left:20px;">
        <strong>Dê sugestões, a fim de que o IFCE melhore a qualidade do seu ensino.</strong><br />
        {{ $data['internshipGeneralSuggestion'] }}
    </p>

    <div style="text-align:center; margin:40px auto 0 auto;">
        _______________________,_____ de ___________________ de _______
    </div>

    <br /> <br />

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) aluno(a)</strong>
    </div>

    @endif

</div> <!-- internshipdocs-content -->

@endsection
