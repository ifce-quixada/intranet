@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
  .contrato {
    width: 90%;
    margin: 0 auto;
  }
  .contrato p {
    text-align: justify;
    line-height: 1.3em;
  }
  .contrato ul li {
    text-align: justify;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-1 -->
    <table width="100%" id="header" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        MATRÍCULA NO ESTÁGIO<br /> <br />
    </h3>

    <table width="100%" id="dados-pessoais" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong>DADOS DO(A) ALUNO(A)</strong></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Nome:</strong> {{ $data['studentName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>E-mail:</strong> {{ $data['studentEmail'] }}</td>
            <td align="left" colspan="1"><strong>Tel:</strong> {{ $data['studentPhone'] }}</td>
            <td align="left" colspan="1"><strong>Data de nasc.:</strong> {{ $data['studentBirthday'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>RG:</strong> {{ $data['studentRG'] }}</td>
            <td align="left" colspan="1"><strong>CPF:</strong> {{ $data['studentCPF'] }}</td>
            <td align="left" colspan="1"><strong>Matrícula:</strong> {{ $data['studentRegistration'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
            <td align="left" colspan="1"><strong>Semstre:</strong> {{ $data['studentSemester'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral dados-pessoais -->

    <table width="100%" id="endereco-aluno" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="1"><strong>Endereço:</strong> {{ $data['studentAddress'] }}</td>
            <td align="left" colspan="1"><strong>Número:</strong> {{ $data['studentAddressStreetNumber'] }}</td>
            <td align="left" colspan="1"><strong>Complemento:</strong> {{ $data['studentAddressSuplement'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Bairro:</strong> {{ $data['studentNeighborhood'] }}</td>
            <td align="left" colspan="1"><strong>CEP:</strong> {{ $data['studentPostalCode'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Cidade:</strong> {{ $data['studentCity'] }}</td>
            <td align="left" colspan="1"><strong>Estado:</strong> {{ $data['studentState'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral endereco-aluno -->

    <table width="100%" id="dados-estagio" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="1"><strong>Período do estágio:</strong> {{ $data['internshipPeriod'] }}</td>
            <td align="left" colspan="1"><strong>Turno(s):</strong> {{ $data['internshipShift'] }}</td>
            <td align="left" colspan="1"><strong>Horário:</strong> {{ $data['internshipTime'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral dados-estagio -->

    <table width="100%" id="dados-da-entidade" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3" style="text-align:center"><strong><br /><br />DADOS DA ENTIDADE</strong></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Empresa:</strong>{{ $data['companyName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Endereço:</strong> {{ $data['companyAddress'] }}</td>
            <td align="left" colspan="1"><strong>Número:</strong> {{ $data['companyAddressStreetNumber'] }}</td>
            <td align="left" colspan="1"><strong>Complemento:</strong> {{ $data['companyAddressSuplement'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Bairro:</strong> {{ $data['companyNeighborhood'] }}</td>
            <td align="left" colspan="1"><strong>CEP:</strong> {{ $data['companyPostalCode'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Cidade:</strong> {{ $data['companyCity'] }}</td>
            <td align="left" colspan="1"><strong>Estado:</strong> {{ $data['companyState'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral dados-da-entidade -->

      <table width="100%" id="dados-do-supervisor" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
          <td align="left" colspan="1"><strong>Período do estágio:</strong> {{ $data['internshipPeriod'] }}</td>
          <td align="left" colspan="1"><strong>Turno(s):</strong> {{ $data['internshipShift'] }}</td>
          <td align="left" colspan="1"><strong>Horário:</strong> {{ $data['internshipTime'] }}</td>
        </tr>

        <tr>
          <td align="left" colspan="2"><strong>Nome do supervisor:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
          <td align="left" colspan="1"><strong>Cargo:</strong> {{ $data['companySupervisorPosition'] }}</td>
          <td align="left" colspan="1"><strong>Telefone:</strong> {{ $data['companySupervisorPhone'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral dados-do-supervisor -->

    <table width="100%" id="dados-do-professor" class="ficha-cadastral" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="2" style="text-align:center"><strong><br /><br />DADOS DO(A) PROFESSOR(A) ORIENTADOR(A) DO ESTÁGIO</strong></td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Nome:</strong> {{ $data['advisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>E-mail:</strong> {{ $data['advisorEmail'] }}</td>
            <td align="left" colspan="1"><strong>Tel:</strong> {{ $data['advisorPhone'] }}</td>
        </tr>
    </table> <!-- ficha-cadastral dados-do-professor -->

    <br /> <br /> <br />

    <table width="60%" id="assinatura-aluno" class="ficha-cadastral" border="0" color="black" cellpadding="0px" style="margin:0 auto;">
        <tr>
            <td align="left" colspan="2" style="text-align:center;border-top:1px solid #000;padding:10px;">
                <strong>Assinatura do aluno</strong>
            </td>
        </tr>
    </table> <!-- ficha-cadastral assinatura-aluno -->

    <!-- nova pagina -->
    <div class="page-break"></div>

    <table width="100%" id="header-2" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <p style="text-align:center">Espaço Reservado ao IFCE</p>
    <div class="espaco-reservado-1">
        1. Aluno(a) matriculado(a) no estágio em _____/_____/_____

        <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
            <strong>Assinatura do servidor</strong>
        </div>

        <p><strong>Observações:</strong><br /></p>
        <p>
            1. Autorizo o aproveitamento das atividades laborais para fim de cumprimento da disciplina
            Estágio Supervisionado, em razão da compatibilidade das atividades desempenhadas
            com a área de formação do(a) aluno(a).<br />

            Carga horária diária para contabilização:
        </p>
        <div style="border-bottom: 1px solid #000;height:12px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>

        <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:80px auto 0 auto;">
            <strong>Assinatura e carimbo do professor orientador</strong>
        </div>

        <p>
            2. Autorizo a antecipação da matrícula do(a) aluno(a) na disciplina de Estágio
            Supervisionado em razão de:
        </p>
        <div style="border-bottom: 1px solid #000;height:12px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>
        <div style="border-bottom: 1px solid #000;height:24px;"></div>

        <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:80px auto 0 auto;">
            <strong>Assinatura e carimbo do professor orientador</strong>
        </div>

    </div> <!-- espaco-reservado-1 -->
    <!-- fim anexo-1 -->

    <!-- nova pagina -->
    <div class="page-break"></div>

    <!-- anexo-8 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        TERMO DE COMPROMISSO DE ESTÁGIO<br /> <br />
    </h3>

    <div class="contrato parte-1">
        <p>
            Em conformidade com a Lei nº 11.788, de 25/09/2008, o INSTITUTO
            FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ, CAMPUS {{
            config('app.campus') }}, interveniente obrigatório neste instrumento,
            representado por encarregado do setor de Estágio do Campus doravante denominado,
            simplesmente, IFCE, e do outro lado, a empresa {{ $data['companyName'] }}, CNPJ
            Nº {{ $data['companyCNPJ']}}, situada a {{ $data['companyAddress']}}, nº {{
            $data['companyAddressStreetNumber'] }}
            @if ($data['companyAddressSuplement'])
                - {{ $data['companyAddressSuplement'] }}
            @endif
            Bairro {{ $data['companyNeighborhood'] }}, CEP: {{
            $data['companyPostalCode'] }}, Telefone: {{ $data['companyPhone'] }}, ramo de
            atividade {{ $data['companyField'] }}, e-mail
            {{ $data['companyEmail'] }}, doravante designada PARTE
            CONCEDENTE e o(a) estagiário(a) {{ $data['studentName'] }}, CPF Nº {{
            $data['studentCPF'] }}, data de nascimento {{ $data['studentBirthday'] }}
            residente na rua {{ $data['studentAddress']}}, nº {{ $data['studentAddressStreetNumber'] }}
            @if ($data['studentAddressSuplement'])
                - {{ $data['studentAddressSuplement'] }}
            @endif
            Bairro {{ $data['studentNeighborhood'] }}, {{ $data['studentCity'] }},
            CEP: {{ $data['studentPostalCode'] }}, aluno(a) do Curso de {{ $data['studentCourse'] }},
            Semestre {{ $data['studentSemester'] }}, desta instituição de ensino, resolvem firmar o presente
            Termo de Compromisso de estágio, mediante as cláusulas e condições a seguir estabelicidas:
        </p>

        <ul>
            <li>
                PRIMEIRA – As atividades desenvolvidas pelo estagiário devem
                ser compatíveis com a formação recebida no Curso, conforme plano de atividades
                em anexo.
            </li>
            <li>
            SEGUNDA – Caberá à parte concedente:
                <ol type="a">
                    <li>
                        Oferecer ao estagiário condições de desenvolvimento vivencial,
                        treinamento prático e de relacionamento humano com observância
                        do plano de atividades do estagiário que passa a ser parte integrante
                        deste documento;
                    </li>
                    <li>
                        Proporcionar à instituição de ensino condições para o
                        aprimoramento e avaliação do estagiário.
                    </li>
                    <li>
                        Designar profissional qualificado como supervisor do estagiário.
                    </li>
                    <li>
                        Estabelecer nos períodos de atividades acadêmicas redução de
                        pelo menos a metade da jornada a ser cumprida em estágio.
                    </li>
                    <li>
                        Conceder período de 30 dias de recesso ao estagiário sempre que o estágio
                        tenha duração igual ou superior a 01(um) ano ou proporcional quando de duração
                        inferior a ser gozado preferencialmente durante as férias escolares.
                    </li>
                    <li>
                        Fornecer, por ocasião do desligamento do estagiário, termo de
                        realização do estágio com indicação resumida das atividades
                    </li>
                </ol>
            </li>
            <li>
                TERCEIRA – Caberá ao Estagiário:
                <ol>
                    <li>Cumprir as atividades estabelecidas pela parte concedente de acordo com a cláusula primeira;</li>
                    <li>Observar as normas internas da parte concedente;</li>
                    <li>Cumprir as instruções contidas no Manual do Estagiário elaborado pela instituição de ensino.</li>
                </ol>
            </li>
            <li>
                QUARTA – O Horário do estágio será de  {{ $data['internshipTime'] }} perfazendo {{ $data['internshipTimeDuration'] }}
                horas semanais devendo esta jornada ser compatível com o horário escolar do estagiário.
            </li>
        </ul>

    </div>

    <!-- nova pagina -->
    <div class="page-break"></div>

    <!-- anexo-8 pag-2 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <div class="contrato parte-2">
        <ul>
            <li>
                QUINTA -  Este Termo de Compromisso terá vigência de {{ $data['internshipPeriod'] }},
                podendo ser rescindido a qualquer tempo, unilateralmente, mediante comunicação escrita,
                independente de pré-aviso, inexistindo qualquer indenização e vínculo de emprego.
            </li>
            <li>
                @if ($data['internshipCompensation'])
                SEXTA – A parte concedente remunerará mensalmente o estagiário através de uma bolsa-auxílio,
                no valor <span style="color:#fff">_</span> de R$ {{ $data['internshipCompensationValue'] }} ({{ $data['internshipCompensationWrittenValue'] }})
                    @if ($data['internshipTransportAssistance'])
                        e Auxílio-transporte no valor de R$ {{ $data['internshipTransportAssistanceValue'] }}
                        ({{ $data['internshipTransportAssistanceWrittenValue'] }})
                    @endif
                    .
                @else
                SEXTA – A parte concedente não remunerará mensalmente o estagiário.
                @endif
            </li>
            <li>
                @if ($data['internshipMode'])
                    SÉTIMA - O IFCE, neste ato, oferece ao estagiário seguro contra acidentes pessoais,
                    com cobertura limitada ao local e período de estágio, mediante apólice nº 16.0982.55524.001
                    da Companhia MBM SEGURADORA S.A.
                @else
                    SÉTIMA - A parte concedente, neste ato, oferece ao estagiário seguro contra
                    acidentes pessoais, com cobertura limitada ao local e período de estágio, mediante
                    apólice nº {{ $data['internshipEnsurance'] }} da Companhia {{ $data['internshipEnsuranceCompany'] }}
                @endif
                comprovado mediante fotocópia da apólice.
            </li>
            <li>
                OITAVA – A Empresa designa o funcionário {{ $data['companySupervisorName'] }}
                cargo/qualificação: {{ $data['companySupervisorPosition'] }} para ser o
                supervisor(a) interno do(a) estagiário(a), que ficará responsável pelo
                acompanhamento e programação das atividades a serem desempenhas no estágio.
            </li>
            <li>
                NONA – Constituem motivos para cessação automática do presente
                Termo de Compromisso:
                <ul>
                    <li>A conclusão ou abandono do estágio ou cancelamento de matrícula.</li>
                    <li>O não cumprimento das cláusulas estabelecidas neste documento.</li>
                    <li>O trancamento ou o abandono do semestre ou do curso.</li>
                    <li>A conclusão do curso.</li>
                    <li>Não frequência às aulas.</li>
                    <li>Pedido de rescisão por parte do aluno ou da parte concedente.</li>
                </ul>
            </li>
        </ul>
        <p>
            Estando de acordo com o que ficou acima expresso, vai o presente instrumento assinado,
            em três vias de igual teor, pelas partes.
        </p>

        <div style="text-align:center; margin:40px auto 0 auto;">
            _______________________,_____ de ___________________ de _______
        </div>

    </div> <!-- contrato parte-2 -->

    <table width="100%" align="center">
        <tr>
            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Empresa<br />(Assinatura e carimbo)
            </td>

            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Aluno(a) Estagiário(a)/Bolsista<br/>(Assinatura)
            </td>

            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Instituição de Ensino - Coord. de Estágio<br/>(Assinatura e carimbo)
            </td>
        </tr>
    </table>

    <!-- fim anexo-8 -->

    <!-- anexo-9 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        PLANO DE ATIVIDADES DO(A) ESTAGIÁRIO(A)<br /> <br />
    </h3>

    <table width="100%" id="relatorio-periodico-atividades" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong><br /><br />1. IDENTIFICAÇÃO DA EMPRESA E SUPERVISOR<br/><br/></strong></td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Nome da empresa:</strong> {{ $data['companyName'] }}</td>
            <td align="left" colspan="1"><strong>CNPJ:</strong> {{ $data['companyCNPJ'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Endereço:</strong> {{ $data['companyAddress'] }}</td>
            <td align="left" colspan="1"><strong>Número:</strong> {{ $data['companyAddressStreetNumber'] }}</td>
            <td align="left" colspan="1"><strong>Complemento:</strong> {{ $data['companyAddressSuplement'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Telefone:</strong> {{ $data['companyPhone'] }}</td>
            <td align="left" colspan="1"><strong>E-mail:</strong> {{ $data['companyEmail'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Bairro:</strong> {{ $data['companyNeighborhood'] }}</td>
            <td align="left" colspan="1"><strong>CEP:</strong> {{ $data['companyPostalCode'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Cidade:</strong> {{ $data['companyCity'] }}</td>
            <td align="left" colspan="1"><strong>Estado:</strong> {{ $data['companyState'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Supervisor do Estágio designado pela Empresa:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Cargo/Qualificação:</strong> {{ $data['companySupervisorPosition'] }}</td>
            <td align="left" colspan="1"><strong>Telefone:</strong> {{ $data['companySupervisorPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong><br /><br />2. IDENTIFICAÇÃO DO(A) ESTAGIÁRIO(A)<br/><br/></strong></td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Nome:</strong> {{ $data['studentName'] }}</td>
            <td align="left" colspan="1"><strong>Telefone:</strong> {{ $data['studentPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
            <td align="left" colspan="1"><strong>Semestre:</strong> {{ $data['studentSemester'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Período do estágio:</strong> {{ $data['internshipPeriod'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Setor de realização do estágio:</strong> {{ $data['internshipExecution'] }}</td>
        </tr>
        <tr>
        </tr>
    </table> <!-- relatorio periodico atividades -->

    <table width="100%" id="identificacao-da-instituicao" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong><br /><br />3. IDENTIFICAÇÃO DA INSTITUIÇÃO DE ENSINO<br/><br/></strong></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Campus:</strong> {{ ucwords(config('app.campus', 'Fortaleza')) }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Professor(a) orientador(a):</strong> {{ $data['advisorName'] }}</td>
            <td align="left" colspan="1"><strong>Telefone:</strong> {{ $data['advisorPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>E-mail do professor orientador:</strong>{{ $data['advisorEmail'] }}</td>
        </tr>
    </table>

    <!-- nova-pagina -->
    <div class="page-break"></div>

    <!-- anexo-9 pagina-2 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <table width="100%" id="atividades-desenvolvidas-no-estagio" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong><br /><br />4. ATIVIDADES A SEREM DESENVOLVIDAS NO ESTÁGIO</strong></td>
        </tr>
    </table>
    <p>
        {{ $data['internshipActivities'] }}
    </p>

    <table width="100%" id="resultados-esperados" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong><br /><br />5. RESULTADOS ESPERADOS</strong></td>
        </tr>
    </table>
    <p>
        {{ $data['internshipResults'] }}
    </p>

    <br/> <br/> <br/> <br/> <br/> <br/>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura e carimbo do(a) supervisor(a) da parte concedente</strong>
    </div>

    <br/> <br/> <br/>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura do(a) estagiario(a)</strong>
    </div>

    <br/> <br/> <br/>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        <strong>Assinatura e carimbo do(a) professor(a) orientador(a) IFCE</strong>
    </div>
    <!-- fim anexo-9 -->

</div> <!-- begin-internshipdocs-content -->

@endsection
