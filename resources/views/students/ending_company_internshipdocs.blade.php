@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-7 pagina 1 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        TERMO DE REALIZAÇÃO DO ESTÁGIO<br /> <br />
    </h3>

    <table width="100%" id="relatorio-final-atividades-cursos-tecnicos" border="0" color="black" cellpadding="0px">
        <tr>
            <td align="left" colspan="3"><strong>Nome:</strong> {{ $data['studentName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Matrícula:</strong> {{ $data['studentRegistration'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Data de nascimento:</strong> {{ $data['studentBirthday'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>RG:</strong> {{ $data['studentRG'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>CPF:</strong> {{ $data['studentCPF'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>E-mail:</strong> {{ $data['studentEmail'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Endereço:</strong>
                {{ $data['studentAddress'] }}, {{ $data['studentAddressStreetNumber'] }}
                @if ($data['studentAddressSuplement'] != '-')
                    - compl. {{ $data['studentAddressSuplement'] }}
                @endif
            </td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Telefone:</strong> {{ $data['studentPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Curso:</strong> {{ $data['studentCourse'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3" style="text-align:center"><br /></td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Parte concedente:</strong> {{ $data['companyName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Telefone:</strong> {{ $data['companyPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Endereço:</strong>
                {{ $data['companyAddress'] }}, {{ $data['companyAddressStreetNumber'] }}
                @if ($data['companyAddressSuplement'] != '-')
                    - compl. {{ $data['companyAddressSuplement'] }}
                @endif
                - Bairro: {{ $data['companyNeighborhood'] }}
                - CEP: {{ $data['companyPostalCode'] }}
            </td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Cidade:</strong> {{ $data['companyCity'] }}</td>
            <td align="left" colspan="1"><strong>Estado:</strong> {{ $data['companyState'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="2"><strong>Supervisor da do estágio na parte concedente:</strong> {{ $data['companySupervisorName'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="1"><strong>Telefone do supervisor:</strong> {{ $data['companySupervisorPhone'] }}</td>
        </tr>

        <tr>
            <td align="left" colspan="3"><strong>Ramo de atividade da parte concedente:</strong> {{ $data['companyField'] }}</td>
        </tr>

    </table> <!-- relatorio-final-atividades-cursos-tecnicos  -->

    <p>
        <strong>Relacione as principais tarefas executadas pelo(a) estagiário(a): </strong><br />
        {{ $data['activitiesPerformedByIntern'] }}
    </p>

    <p>
        <strong>Desempenho funcional do(a) estagiário(a): </strong><br />
    </p>

    <table width="80%" id="desempenho-funcional-estagiario" border="0" color="black" cellpadding="0px" style="margin:0 auto;">
        <tr>
            <td></td>
            <td style="text-align:center;">Ótimo</td>
            <td style="text-align:center;">Bom</td>
            <td style="text-align:center;">Regular</td>
            <td style="text-align:center;">Insuficiente</td>
        </tr>
        <tr>
            <td>2.1 - Aprendizagem</td>
            <td style="text-align:center;">( {{ $data['scoreLearning'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLearning'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLearning'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreLearning'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.2 - Segurança na execução do trabalho</td>
            <td style="text-align:center;">( {{ $data['scoreSecurity'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSecurity'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSecurity'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreSecurity'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.3 - Interesse</td>
            <td style="text-align:center;">( {{ $data['scoreInterest'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterest'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterest'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterest'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.4 - Iniciativa Própria</td>
            <td style="text-align:center;">( {{ $data['scoreProactivity'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProactivity'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProactivity'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProactivity'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.5 - Conhecimentos técnicos</td>
            <td style="text-align:center;">( {{ $data['scorePracticalKnowledge'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePracticalKnowledge'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePracticalKnowledge'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePracticalKnowledge'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.6 - Qualidade/Produtividade</td>
            <td style="text-align:center;">( {{ $data['scoreProductivity'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProductivity'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProductivity'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreProductivity'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.7 - Disciplina</td>
            <td style="text-align:center;">( {{ $data['scoreDiscipline'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreDiscipline'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreDiscipline'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreDiscipline'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>
    </table> <!-- desempenho funcionado do estagiario -->

    <!-- anexo-4 pagina-2 -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <table width="80%" id="desempenho-funcional-estagiario-parte-2" border="0" color="black" cellpadding="0px" style="margin:0 auto;">
        <tr>
            <td></td>
            <td style="text-align:center;">Ótimo</td>
            <td style="text-align:center;">Bom</td>
            <td style="text-align:center;">Regular</td>
            <td style="text-align:center;">Insuficiente</td>
        </tr>
        <tr>
            <td>2.8 - Relacionamento Interpessoal</td>
            <td style="text-align:center;">( {{ $data['scoreInterpersonalRelationship'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterpersonalRelationship'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterpersonalRelationship'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreInterpersonalRelationship'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.9 - Assume a responsabilidade de seus atos</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsability'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsability'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsability'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreTakesResponsability'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.10 - Pontualidade</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scorePunctuality'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>

        <tr>
            <td>2.11 - Assiduidade</td>
            <td style="text-align:center;">( {{ $data['scoreAttendance'] == "awesome" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreAttendance'] == "good" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreAttendance'] == "regular" ? "X" : "  "}} )</td>
            <td style="text-align:center;">( {{ $data['scoreAttendance'] == "insufficient" ? "X" : "  "}} )</td>
        </tr>
    </table> <!-- desempenho funcional do estagiario - parte 2 -->

    <p>
        <strong>A parte concedente faz a avaliação do estágio através de: </strong><br />
        {{ $data['avaliationMethods'] }}
    </p>

    <p>
        4. O aluno cumpriu nesta parte concedente {{ $data['internshipTotalHours'] }}  horas de estágio
        no período de {{ $data['internshipPeriod'] }}.
    </p>

    <p>
        <strong>
        5.  Apresente sugestões para alteração de matrizes curriculares e ofertas de novos cursos:
        </strong><br />
        {{ $data['generalSuggestions'] }}
    </p>

    <div style="width:60%;text-align:center;border-top:1px solid #000;padding:10px;margin:40px auto 0 auto;">
        Supervisor do(a) estagiário(a) na parete concedente<br /><strong>(Carimbo e Assinatura)</strong>
    </div>

    <div style="width:80%;border:1px solid #000;padding:0px;margin:80px auto 0 auto;text-align:center;">
        CARIMBO COM CNPJ DA PARTE CONCEDENTE OU COM O NÚMERO DO REGISTRO NO CONSELHO DE FISCALIZAÇÃO PROFISSIONAL
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>

</div> <!-- internshipdocs-content -->
@endsection
