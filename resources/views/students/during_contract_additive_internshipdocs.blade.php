@extends('layouts.studentinternship')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 2cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    float: none !important;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm;
  }
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  .ficha-cadastral {
    margin-bottom: 0.4cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header, #header-2,.logo {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h3, h4, h5 {
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
  }
  .brasao-federal-2-quixada {
    width: 281px;
    height: 90px;
    margin: 0 auto;
    padding-bottom: 290px;
  }
  .espaco-reservado-1 {
    padding: 10px;
    border: 1px solid #000;
  }
  #horas-trabalhadas, #horas-trabalhadas td,
  #horas-trabalhadas-pag-2, #horas-trabalhadas-pag-2 td {
    border: 1px solid #000;
    border-collapse: collapse;
  }
</style>
@endsection

@section('content')

<div id="internshipdocs-content">

    <!-- anexo-2 -->
    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>
    <h3>
        CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}
    </h3>
    <h3>
        TERMO ADITIVO<br /> <br />
    </h3>

    <div class="contrato parte-1">
        <p style="line-height:1.5em">
            Pelo presente instrumento particular, decidem as partes alterar, em observância à Lei n.º 11.788/2008, o
            Termo de Compromisso de Estágio celebrado entre o Instituto Federal de Educação, Ciência e Tecnologia do Ceará
            – campus {{ config('app.campus') }}, a empresa concedente {{ $data['companyName'] }}, e o(a) estudante
            {{ $data['studentName'] }}, regularmente matriculado(a) no Curso {{ $data['studentCourse'] }}, com matrícula
            {{ $data['studentRegistration'] }}, todos devidamente qualificados no instrumento ora aditado.
        </p>
        <p style="line-height:1.5em">
            Fica(m) alterada(s) a(s) cláusula(s) abaixo, permanecendo as demais de acordo com o Termo de
            Compromisso de Estágio anteriormente firmado:
        </p>

        @if ($data['contractExtension'] == "true")
            <p><strong>(X) Prorrogação do período de estágio:</strong></p>
            <p style="margin-left:20px">O termo de Compromisso do Estágio fica prorrogado de {{ $data['contractPeriod'] }}.</p>
        @else
            <p><strong>(  ) Prorrogação do período de estágio:</strong></p>
            <p>Não há alterações.</p><br />
        @endif

        @if ($data['contractWorkHour'] == "true")
            <p><strong>(X) Alteração do horário das atividades de estágio:</strong></p>
            <p style="margin-left:20px">
                {{ $data['contractHoursGroup1'] }} nos dias {{ $data['contractWeekdays1'] }}.<br />
                @if (@isset($data['contractHoursGroup2']))
                    {{ $data['contractHoursGroup2'] }} nos dias {{ $data['contractWeekdays2'] }}.<br />
                @endif
                @if (@isset($data['contractHoursGroup3']))
                    {{ $data['contractHoursGroup3'] }} nos dias {{ $data['contractWeekdays3'] }}.<br />
                @endif
        @else
            <p><strong>(  ) Alteração do horário das atividades de estágio:</strong></p>
            <p>Não há alterações.</p><br />
        @endif

        @if ($data['contractCompensation'] == "true")
            <p><strong>(X) Alteração do valor da bolsa de Complementação Educacional:</strong></p>
            <p style="margin-left:20px;">
                A partir de {{ $data['contractCompensationNewDate']}}, a concedente altera a bolsa-auxílio para o valor de
                R$ {{ $data['contractCompensationValue'] }} ({{ $data['contractCompensationWrittenValue'] }}).
            </p>
        @else
            <p><strong>(  ) Alteração do valor da bolsa de Complementação Educacional:</strong></p>
            <p>Não há alterações.</p><br />
        @endif

        @if ($data['contractSupervisor'] == "true")
            <p><strong>(X) Alteração de supervisão de estágio:</strong></p>
            <p style="margin-left:20px;">
                A partir desta data o(a) estagiário(a) será supervisionado(a) por {{ $data['contractNewSupervisor'] }},
                ocupante do cargo de {{ $data['contractNewSupervisorOccupation'] }}.
            </p>
        @else
            <p><strong>(  ) Alteração de supervisão de estágio:</strong></p>
            <p>Não há alterações.</p><br />
        @endif
    </div> <!-- parte-1 -->

    <!-- nova pagina -->
    <div class="page-break"></div>

    <table width="100%" class="logo" border="0">
        <tr>
            <td style="text-align:center">
                <img src="{{ public_path() . '/img/brasao-federal.png' }}" alt="logo ifce" width="91" height="90" />
            </td>
        </tr>
    </table> <!-- logo-brasao -->

    <h4>
        INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DO CEARÁ<br />
        CONSELHO SUPERIOR
    </h4>

    <div class="contrato parte-1">

        @if ($data['contractActivities'] == "true")
            <p><strong>(X) Alteração das atividades do estágio:</strong></p>
            <p style="margin-left:20px;">
                A partir desta data, serão confiadas ao estagiário, devidamente supervisionado
                por profissional indicado pela parte concedente e qualificado no plano de
                atividades integrante deste instrumento, as seguintes atividades:
            </p>
            <p style="margin-left:20px;">
                {{ $data['contractNewActivities'] }}
            </p>
        @else
            <p><strong>(  )  Alteração das atividades do estágio:</strong></p>
            <p>Não há alterações.</p><br />
        @endif

        @if ($data['contractOther'] == "true")
            <p><strong>(X) Outra alteração:</strong></p>
            <p style="margin-left:20px;">
                {{ $data['contractNewTerms'] }}.
            </p>
        @else
            <p><strong>(  ) Outra alteração:</strong></p>
            <p>Não há alterações.</p><br />
        @endif

        <p>
            Permanecem inalteradas e em vigor as demais disposições do Termo de Compromisso de Estágio firmado
            entre a Concedente, o(a) Estagiário(a) e a Instituição de Ensino.
        </p>

        <p>
            E por assim estarem justas e combinadas, assinam o presente instrumento em 3 (três) vias de igual teor,
            forma e validade.
        </p>
    </div>


    <div class="contrato parte-2">
        <div style="text-align:center; margin:40px auto 0 auto;">
            _______________________,_____ de ___________________ de _______
        </div>
    </div>

    <br> <br> <br>

    <table width="100%" align="center">
        <tr>
            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Empresa<br />(Assinatura e carimbo)
            </td>

            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Aluno(a) Estagiário(a)/Bolsista<br/>(Assinatura)
            </td>

            <td>
                <div style="width:90%;text-align:center;border-top:1px solid #000;margin:80px auto 0 auto;">
                Instituição de Ensino - Coord. de Estágio<br/>(Assinatura e carimbo)
            </td>
        </tr>
    </table>

    <br> <br> <br>

    <p style="text-align:center">
        (ESTE DOCUMENTO DEVE SER IMPRESSO FRENTE E VERSO PARA TER VALIDADE)
    </p>
    <p style="text-align:center">
        (NO CASO DE PRORROGAÇÃO DO PERÍODO DO ESTÁGIO DEVE SER PREENCHIDO TAMBÉM O ESPAÇO
        RESERVADO PARA ALTERAÇÕES NAS ATIVIDADES DO ESTÁGIO)
    </p>

</div> <!-- internshipdocs-content -->

@endsection
