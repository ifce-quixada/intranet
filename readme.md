## Sobre Intranet

Esse projeto é a API do sistema de intranet do Campus Quixadá.
A Intranet serve para auxiliar os servidores administrativos no dia
a dia da instituição. O sistema foi desenvolvido utilizando o framework
[Laravel](https://laravel.com) (linguagem de programação PHP). Será necessário
um banco de dados, pode ser [Mariadb](https://mariadb.org/) ou
[PostgreSQL](https://postgresql.org).

## Como faço para implantar o sistema no meu campus?

Existem, basicamente, duas maneiras de se implantar o sistema de Intranet
no seu Campus.

### 1a maneira

O primeiro deles é criando uma máquina GNU/Linux (virtual ou não) na sua
infraestrutura de TI e instalando as dependências necessárias do projeto. Você
vai precisar de um servidor web (nginx ou apache por exemplo), PHP >= 7.2
(juntamente com o PHP-FPM) e o banco de dados.

### 2a maneira

A segunda maneira, e mais simples, é utilizando a imagem Docker utilizando o
arquivo docker-compose desse repositório. Para isso você também vai precisar de
uma máquina GNU/Linux (virtual ou não) na sua infraestrutura de TI e, como
dependência, o Docker e o docker-compose.

## Como faço para contribuir?

É muito fácil contribuir com o projeto. Faça um fork desse repositório,
desenvolva suas modificações e em seguida faça um *pull-request* nesse projeto.

Softwares necessários para contribuir:

* Composer
* PHP >= 7.0 < 7.2 (módulos fpm, mbstring, xml, gd, mysql|pgsql, ldap)
* Mariadb >= 10.2.12 ou PostgreSQL >= 10.1

Para iniciar, você vai precisar executar os seguintes comandos (entre
no diretório em que o projeto está salvo):

```
composer self-update
composer install
cp .env.example .env (edite as informações necessárias)
php artisan cache:clear
php artisan key:generate
php artisan migrate
php artisan storage:link
php artisan serve (servir a aplicação em ambientes de desenvolvimento)
```

A autenticação acontece por meio do protocolo ldap, é necessário, portanto, que
o desenvolvedor tenha disponível um servidor LDAP ou acesso ao Active Directory
da Reitoria com as informações abaixo, que devem ser requisitadas por meio da
Central de Atendimento da Reitoria:

```
LDAP_LOGGING=true
LDAP_CONNECTION=default
LDAP_HOST=<subdominio>.ifce.edu.br
LDAP_USERNAME="cn=<campus>,cn=users,dc=<subdominio>,dc=ifce,dc=edu,dc=br"
LDAP_PASSWORD="<senha>"
LDAP_PORT=389
LDAP_BASE_DN="ou=IFCE,dc=<subdominio>,dc=ifce,dc=edu,dc=br"
LDAP_TIMEOUT=5
LDAP_SSL=false
LDAP_TLS=false
```

## Versão atual

A versão atual da Intranet é v0.9.0.

## Licença

TODO: escolher uma licença
